package org.indrasoftwarelabs.containers;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ScrollPaneConstants;

import org.indrasoftwarelabs.db.Query;
import org.indrasoftwarelabs.db.Sentence;
import org.indrasoftwarelabs.log.LogForDay;
import org.indrasoftwarelabs.main.MainForm;
import org.indrasoftwarelabs.main.QCS;

@SuppressWarnings("serial")
public class ModuleMethodContainer extends Container {
	
	Container miContenedor = new Container();
	
	public final static int MODULO = 1;
	public final static int METODO = 2;
	
	/**
	 * Get Container
	 * @return
	 */
	public Container getMyContainer() {
		return miContenedor;
	}

	public ModuleMethodContainer(final int type, final boolean insertMode, final JFrame frame, final Properties prop, final Container mainContenedor, final String campoEditable, final JList listMod, final JList listMet, final JList listSen){
		
		miContenedor.setLayout(null);
		
		String tipo = null;
		
		switch(type){
			case MODULO:
				tipo = "Modulo:";
				break;
			case METODO:
				tipo = "Metodo:";
				break;
		}
		
		JLabel etiqueta = new JLabel();
		
        etiqueta = new JLabel();
		etiqueta.setText(tipo);
        etiqueta.setBounds(20, 95, 80, 30); // x, y, ancho, alto
        miContenedor.add(etiqueta);
		
        etiqueta = new JLabel();
		etiqueta.setText("Descripcion:");
        etiqueta.setBounds(20, 145, 100, 30); // x, y, ancho, alto
        miContenedor.add(etiqueta);
        
        final JTextField campoNombre = new JTextField();
        
        if(campoEditable != null){
        	campoNombre.setText(campoEditable);
        }
        campoNombre.setSize(300, 30);
        campoNombre.setBounds(120, 100, 300, 30); // x, y, ancho, alto
        campoNombre.setEditable(true);
        campoNombre.setVisible(true);
        miContenedor.add(campoNombre);
        
        final JTextArea areaDesc = new JTextArea(40,30);
        areaDesc.setLineWrap(true);
        areaDesc.setWrapStyleWord(true);
        areaDesc.setEditable(true);
        areaDesc.setBounds(120, 145, 300, 100); // x, y, ancho, alto
        areaDesc.setAutoscrolls(true);
        
	    if( type == ModuleMethodContainer.MODULO ){
        	areaDesc.setEnabled(false);
        	areaDesc.setBackground(Color.LIGHT_GRAY);
        	miContenedor.add(areaDesc);
        }else{
        	
        	if(campoEditable != null){
            	
        		LogForDay.writeToLog("Estableciendo conexion a BBDD ", QCS.logPath, LogForDay.DEBUG);
        		Connection con = null;
				try {
					con = MainForm.getConexion(prop);
				} catch (SQLException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					return;
				} catch (ClassNotFoundException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					return;
				} catch (Exception err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					return;
				}
        		
        		// Se busca la descripcion del metodo: campoEditable
        		try {
        			String query = "SELECT METHOD_DESCRIPTION FROM GCQS_METHOD WHERE METHOD_NAME = '?'";
        			query = query.replaceFirst("\\?", campoEditable);
					ResultSet rs = Query.executeQueryByConn(query, con);
					rs.next();
					areaDesc.setText(rs.getString("METHOD_DESCRIPTION"));
				} catch (SQLException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);	
				} catch (ClassNotFoundException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);	
				} catch (Exception err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);	
				}
				
				try {
					MainForm.closeConexion(con);
					LogForDay.writeToLog("Conexxion BBDD cerrada", QCS.logPath, LogForDay.DEBUG);
				} catch (SQLException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
				} catch (ClassNotFoundException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
				}
            	
            }
        	
        	JScrollPane sp = new JScrollPane(areaDesc);
    	    sp.setBounds(120, 145, 300, 100); // x, y, ancho, alto
    	    sp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    	    miContenedor.add(sp);
        }
	    
        // VOLVER
	    
	    Icon iconBack = new ImageIcon("res/icons/Home-icon.png");
	    
        JButton botonVolver = new JButton();
        botonVolver.setText("BACK");
        botonVolver.setIcon(iconBack);
        botonVolver.setVerticalTextPosition(JButton.BOTTOM);
        botonVolver.setHorizontalTextPosition(JButton.CENTER);
        botonVolver.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		int value = JOptionPane.showConfirmDialog(frame,"�Deseas volver sin guardar las modificaciones?","VOLVER",JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
        		
        		if(value == JOptionPane.OK_OPTION ){
        			frame.setContentPane(mainContenedor);
                    frame.setVisible(true);
        		}
        		
        	}
        });
        miContenedor.add(botonVolver);
        
        // GUARDAR
        Icon iconSave = new ImageIcon("res/icons/Save-icon.png");
        
        final JButton botonGuardar;
        botonGuardar = new JButton("SAVE");
        botonGuardar.setIcon(iconSave);
        botonGuardar.setVerticalTextPosition(JButton.BOTTOM);
        botonGuardar.setHorizontalTextPosition(JButton.CENTER);
        botonGuardar.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		String valor = "";
        		if(campoNombre.getText() != null){
        			valor = campoNombre.getText().replaceAll(" ", "");
        		}
        		
        		// Si no se ha introducido un nombre, se muestra una advertencia 
        		if(	campoNombre.getText() == null || 
        			campoNombre.getText().length() <= 0 ||
        			valor == null ||
        			valor.length() <= 0 ){
        			String nombre = null;
        			switch(type){
        			case MODULO:
        				nombre = "modulo";
        				break;
        			case METODO:
        				nombre = "metodo";
        				break;
        			}
        			String msg = "Es obligatorio introducir un nombre para el " + nombre;
        			JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
        			return;
        		}
        		
        		int value = JOptionPane.showConfirmDialog(frame,"�Deseas guardar las modificaciones?","GUARDAR",JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
				if(value == JOptionPane.OK_OPTION ){
					
					LogForDay.writeToLog("Estableciendo conexion a BBDD ", QCS.logPath, LogForDay.DEBUG);
	        		Connection con;
					try {
						con = MainForm.getConexion(prop);
					} catch (SQLException err) {
						LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
						return;
					} catch (ClassNotFoundException err) {
						LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
						return;
					} catch (Exception err) {
						LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
						return;
					}
					
					// Se guarda en BBDD el nombre y la descripcion del modulo o metodo
					String nombre		= null;
					String descripcion	= null;
					String query		= null;
					int rowsAffected 	= 0;
					
					nombre		= 	campoNombre.getText();
					descripcion	=	areaDesc.getText();
					
        			switch(type){
	        			case MODULO:
	        				
	        				if(insertMode){
	        					query = Sentence.INSERT_MODULO.replaceFirst("\\?", nombre);
		        				
								try {
									rowsAffected = Query.executeInsert(query, con);
								} catch (SQLException err) {
									LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
								} catch (ClassNotFoundException err) {
									LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
								} catch (Exception err) {
									LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
								}
	        				}else{
	        					query = Sentence.EDIT_MODULO.replaceFirst("\\?", nombre);
	        					query = query.replaceFirst("\\?", campoEditable);
		        				
								try {
									rowsAffected = Query.executeInsert(query, con);
								} catch (SQLException err) {
									LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
								} catch (ClassNotFoundException err) {
									LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
								} catch (Exception err) {
									LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
								}
	        				}
	        				
	        				break;
	        				
	        			case METODO:
	        				
	        				if(insertMode){
	        					query = Sentence.INSERT_METODO.replaceFirst("\\?", nombre);
		        				query = query.replaceFirst("\\?", descripcion);
		        				
								try {
									rowsAffected = Query.executeInsert(query, con);
								} catch (SQLException err) {
									LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
								} catch (ClassNotFoundException err) {
									LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
								} catch (Exception err) {
									LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
								}
	        				}else{
	        					query = Sentence.EDIT_METODO.replaceFirst("\\?", nombre);
		        				query = query.replaceFirst("\\?", descripcion);
		        				query = query.replaceFirst("\\?", campoEditable);
		        				
								try {
									rowsAffected = Query.executeInsert(query, con);
								} catch (SQLException err) {
									LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
								} catch (ClassNotFoundException err) {
									LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
								} catch (Exception err) {
									LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
								}
	        				}
	        				
	        				break;
        			}
        			
        			if(rowsAffected == 0){
        				String msg = "Error al insertar en BBDD. Por favor, revisa las tablas en BBDD";
            			JOptionPane.showConfirmDialog(frame,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
            			try {
							con.rollback();
						} catch (SQLException err) {
							LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
							msg = "Error al hacer rollback en BBDD: " + err.getLocalizedMessage();
							JOptionPane.showConfirmDialog(frame,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
                			return;
						}
        			}else{
        				
        				try{
        					con.commit();
        				}catch(SQLException err){
        					
        					LogForDay.writeToLog("ERROR al hacer commit " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
        					
        					String msg = "Error al hacer commit en BBDD: " + err.getLocalizedMessage();
                			JOptionPane.showConfirmDialog(frame,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
                			return;
        				}
        				
        				String msg = "Los cambios se guardaron correctamente";
            			JOptionPane.showConfirmDialog(frame,msg,"",JOptionPane.CLOSED_OPTION, JOptionPane.INFORMATION_MESSAGE);
            			
            			frame.setContentPane(mainContenedor);
                        frame.setVisible(true);
                        
                        switch(type){
		        			case MODULO:
		                        // Refrescar los modulos
								// LISTA DE LOS MODULOS
							ResultSet rs = null;
							try {
								rs = Query.executeSelectByConn(Sentence.GET_ALL_MODULES, con);
							} catch (SQLException err) {
								LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
							} catch (ClassNotFoundException err) {
								LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
							} catch (Exception err) {
								LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
							}
						        String[] modules = null;
						               
						        try {
									rs.last();
									modules = new String[rs.getRow()];
									rs.beforeFirst();
								} catch (SQLException err) {
									LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
								}
						        
						    	try {
						    		int cont = 0;
									while (rs.next()){
										String name =	rs.getString("MODULE_NAME");
										modules[cont] = name;
										cont++;
									}
									rs.close();
								} catch (SQLException err) {
									LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
								}
			        			
								listMod.setListData(modules);
		                        
		        				break;
		        			case METODO:
		        				
		        				// Refrescar los metodos
								// Ejecutar la query y poner los valores a listMet
			                	String nameModule = (String) listMod.getSelectedValue();
			                	String[][] params = Query.sentenceAddParam(Query.TYPE_STRING, nameModule, 1);
			                	
			                	String[] methods = null;
			                	
			                    try {
			                    	
			                    	rs = Query.executeQueryWithParamsByConn(Sentence.GET_TOTAL_METHODS_BY_MODULE_ID, params, con);
			                    	rs.next();
			                    	methods	= new String[rs.getInt("TOTAL")];
			                    	
			                    	rs = Query.executeQueryWithParamsByConn(Sentence.GET_METHODS_BY_MODULE_ID, params, con);
			                		int cont = 0;
			                		
			            			while (rs.next()){
			            				String name =	rs.getString("METHOD_NAME");
			            				methods[cont] = name;
			            				cont++;
			            			}
			            			
			            			rs.close();
			            			Query.stmt.close();
			            			
			            		} catch (SQLException err) {
			            			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			            			methods	= new String[0];
			            		} catch (Exception err) {
			            			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
								}
			            		
			            		// REFRESH VALUES
			                    listMet.setListData(methods);
								
								// Refrescar las queries...
								// Ejecutar la query y poner los valores a listMet
			                	String nameMethod = (String) listMet.getSelectedValue();
			                	params = Query.sentenceAddParam(Query.TYPE_STRING, nameMethod, 1);
			                	String[] queries = null;
			                	
			                    try {
			                    	
			                    	rs = Query.executeQueryWithParamsByConn(Sentence.GET_TOTAL_QUERIES_BY_METHOD_NAME, params, con);
			                    	rs.next();
			                    	queries	= new String[rs.getInt("TOTAL")];
			                    	
			                    	rs = Query.executeQueryWithParamsByConn(Sentence.GET_QUERY_BY_METHOD_NAME, params, con);
			                		int cont = 0;
			                		
			            			while (rs.next()){
			            				String name =	rs.getString("QUERY_NAME");
			            				queries[cont] = name;
			            				cont++;
			            			}
			            			
			            			rs.close();
			            			Query.stmt.close();
			            			
			            		} catch (SQLException err) {
			            			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			            			queries	= new String[0];
			            		} catch (Exception err) {
			            			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
								}
			            		
			            		// REFRESH VALUES
			            		listSen.setListData(queries);
		        				
		        				break;
                        }
                        
        			}
					
        			try {
    					MainForm.closeConexion(con);
    					LogForDay.writeToLog("Conexxion BBDD cerrada", QCS.logPath, LogForDay.DEBUG);
    				} catch (SQLException err) {
    					LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
    				} catch (ClassNotFoundException err) {
    					LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
    				}
        			
				}
        		
        	}
        });
        
        miContenedor.add(botonGuardar);
        
        // TOOLBAR
        JToolBar toolbar = new JToolBar();
        toolbar.setSize(700, 75);
        toolbar.setFloatable(false);
        
        toolbar.add(botonGuardar);
        toolbar.addSeparator(); //SEPARADOR
        toolbar.add(botonVolver);
        
        miContenedor.add(toolbar);
        
	}
	
}
