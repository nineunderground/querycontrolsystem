package org.indrasoftwarelabs.containers;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import org.indrasoftwarelabs.db.Query;
import org.indrasoftwarelabs.db.Sentence;
import org.indrasoftwarelabs.dialogs.ArgumentDialog;
import org.indrasoftwarelabs.dialogs.ResultDialog;
import org.indrasoftwarelabs.log.LogForDay;
import org.indrasoftwarelabs.main.MainForm;
import org.indrasoftwarelabs.main.QCS;

@SuppressWarnings("serial")
public class EditContainer extends Container {

	Container miContenedor = new Container();
	
	ResultSet rs;
	
	String sentenceName;
	String sentenceDescription;
	String sentenceSQL;
	String sentenceSQLFormatted;
	String sentenceArgsMap;
	String sentenceMaxRegisters = "0";
	
	final static int NEW	= 1;
	final static int EDIT	= 2;
	
	int		valueOrder;
	String	queryNAME;
	int		queryID;
	int		mode;
	
	final DefaultTableModel model = new DefaultTableModel();
	
	//Connection con;
	
	List<String[]> listaArgumentos = new ArrayList<String[]>();
	String[] filaParametros = null;
	
	final List<String> vectorParamsIDs = new ArrayList<String>();
	
	String columnNames[] = {"RANK", "NOMBRE", "TIPO", "VALOR"};
	Object[][] rowData = null;
	
	public static final String SEPARADOR = "?*?";
	
	final String DATE_FORMAT_DATE		= "dd/MM/yyyy";
	final String DATE_FORMAT_TIMESTAMP	= "dd/MM/yyyy HH:mm:ss";
	DateFormat dateFormat;
	Date date;
	
	/**
	 * Get Container
	 * @return
	 */
	public Container getMyContainer() {
		return miContenedor;
	}
	
	public EditContainer(String queryName, final JFrame frame, final Properties prop, final Container mainContenedor){
		
		if(queryName == null){
    		mode = NEW;
    	}else{
    		mode = EDIT;
    	}
    	
    	//con = conn;
    	sentenceName = queryName;
    	
    	if(queryName != null){
    		
    		try {
        		
    			LogForDay.writeToLog("Estableciendo conexion a BBDD ", QCS.logPath, LogForDay.DEBUG);
        		Connection con;
				try {
					con = MainForm.getConexion(prop);
				} catch (SQLException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					return;
				} catch (ClassNotFoundException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					return;
				} catch (Exception err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					return;
				}
    			
        		String[][] params = Query.sentenceAddParam(Query.TYPE_STRING, queryName, 1);
    			rs = Query.executeQueryWithParamsByConn(Sentence.GET_QUERY_DATA_BY_QUERY_ID, params, con);
    			while (rs.next()){
    				queryNAME		=	params[0][1];
    				
    				sentenceName		=	rs.getString("NAME");
    				sentenceDescription =	rs.getString("DESCRIPTION");
    				sentenceSQL			=	rs.getString("SQL_SENTENCE");
    				sentenceArgsMap		=	rs.getString("MAP");
    				sentenceMaxRegisters=	String.valueOf(rs.getInt("MAX_REGISTERS"));
    				
    				queryID				=	rs.getInt("ID");
    			}
    			
    			rs = Query.executeQueryWithParamsByConn(Sentence.GET_QUERY_ARGUMENT_DATA_BY_QUERY_ID, params, con);
    			while (rs.next()){
    				filaParametros = new String[4];
    				filaParametros[0] = String.valueOf(rs.getInt("ARGUMENT_ORDER"));
    				filaParametros[1] = rs.getString("ARGUMENT_NAME");
    				filaParametros[2] = rs.getString("ARGUMENT_TYPE");
    				filaParametros[3] = rs.getString("ARGUMENT_ID");
    				
    				listaArgumentos.add(filaParametros);	
    			}
    			
    			sentenceSQLFormatted = sentenceSQL;
    			
    			if(sentenceArgsMap!= null){
    				// Se sustituye cada interrogacion por el argumento que toque la variable sentenceArgsMap = "0,1,1"
    				StringTokenizer token = new StringTokenizer(sentenceArgsMap, ",");
    				while(token.hasMoreTokens()){
    					valueOrder = Integer.parseInt(token.nextToken());
    					filaParametros = listaArgumentos.get(valueOrder);
    					sentenceSQLFormatted = sentenceSQLFormatted.replaceFirst("\\?", filaParametros[1]);
    				}
    			}
    			
    			try {
					MainForm.closeConexion(con);
					LogForDay.writeToLog("Conexxion BBDD cerrada", QCS.logPath, LogForDay.DEBUG);
				} catch (SQLException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
				} catch (ClassNotFoundException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
				}
    			
    		} catch (SQLException err) {
    			LogForDay.writeToLog("Error al hacer rollback " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
    		} catch (Exception err) {
    			LogForDay.writeToLog("Error al hacer rollback " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
    		}
    		
    	}
    	
    	//miContenedor.setSize(700, 800);
    	miContenedor.setLayout(null);
    	
        JLabel	etiqueta;
        final JTextArea areaDesc = new JTextArea(40,30);
        final JTextArea areaSQL = new JTextArea(40,30);
        
        final JButton botonVolver;
        final JButton botonGuardar;
        final JButton botonVerify = new JButton();
        final JButton botonRestaurar;
        
        JButton botonAddParam;
        JButton botonRemoveParam;
        JButton botonEditParam;
        
        final JTextField campoNombre;
        final JTextField campoMax = new JTextField();
        
        final JTable table;
        
        //Object[][] rowData = new Object[listaArgumentos.size()][4];
        rowData = new Object[listaArgumentos.size()][4];
        
        for(int x=0; x<columnNames.length;x++){
        	model.addColumn(columnNames[x]);
        }
        
        for(int x=0; x<listaArgumentos.size();x++){
        	
        	filaParametros = listaArgumentos.get(x);
        	
        	rowData[x][0] = filaParametros[0]; 	// RANK 
        	rowData[x][1] = filaParametros[1]; 	// NOMBRE
        	rowData[x][2] = filaParametros[2]; 	// TIPO
        	
    		if(filaParametros[2].equals("DATE")){
    			dateFormat = new SimpleDateFormat(DATE_FORMAT_DATE);
    	        date = new Date();
    	        rowData[x][3] = dateFormat.format(date);
    		}else if(filaParametros[2].equals("TIMESTAMP")){
    			dateFormat = new SimpleDateFormat(DATE_FORMAT_TIMESTAMP);
    	        date = new Date();
    	        rowData[x][3] = dateFormat.format(date);
    		}else{
    			rowData[x][3] = "0";
    		}
        	
        	model.addRow(rowData[x]);

        	// Coger las colunas en un vector. Sintaxis: NOMBRE_ID_TIPO
        	vectorParamsIDs.add(filaParametros[1] + EditContainer.SEPARADOR + filaParametros[2] + EditContainer.SEPARADOR + filaParametros[3]);
        	
        }
        
        table = new JTable(model){
        	private static final long serialVersionUID = 1L;
			// Editable solo la columna values
			public boolean isCellEditable(int row,int column){
				if(column == 3){
					return true;
				}else{
					return false;
				}
			}
        };
        
        // Single selection
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        // Ancho de las columnas
        TableColumn col;
        
        // Se recorren las columnas para ponerles un ancho fijo
        for(int z=0; z<table.getColumnCount(); z++){
        	col = table.getColumnModel().getColumn(z);
        	
        	switch(z){
        	case 0:
        		col.setPreferredWidth(36);
        		break;
        	case 1:
        		col.setPreferredWidth(144);
        		break;
        	case 2:
        		col.setPreferredWidth(70);
        		break;
        	case 3:
        		col.setPreferredWidth(117);
        	}
        	
        }
        
        // FILA 1
        /*******************************/
        etiqueta = new JLabel("NOMBRE: ");
        etiqueta.setBounds(20, 90, 80, 30); // x, y, ancho, alto
        miContenedor.add(etiqueta);
        
        campoNombre = new JTextField();
        etiqueta.setLabelFor(campoNombre);
        campoNombre.setSize(300, 30);
        campoNombre.setBounds(120, 90, 300, 30); // x, y, ancho, alto
        campoNombre.setText(sentenceName);
        campoNombre.setEditable(true);
        campoNombre.setVisible(true);
        miContenedor.add(campoNombre);
        
        // BOTON
        Icon iconSave;
        Icon iconCheck;
        Icon iconReset;
        Icon iconBack;
        
        iconSave	= new ImageIcon("res/icons/Save-icon.png");
        iconCheck	= new ImageIcon("res/icons/bbdd.png");
        iconReset	= new ImageIcon("res/icons/Refresh-icon.png");
        iconBack	= new ImageIcon("res/icons/Home-icon.png");
        
        botonGuardar = new JButton("SAVE");
        botonGuardar.setIcon(iconSave);
        botonGuardar.setBounds(30, 550, 48, 48); // x, y, ancho, alto
        //botonGuardar.setToolTipText("Guardar");
        botonGuardar.setVerticalTextPosition(JButton.BOTTOM);
        botonGuardar.setHorizontalTextPosition(JButton.CENTER);
        botonGuardar.addActionListener(new ActionListener() {

        	public void actionPerformed(ActionEvent event) {
        		
        		int value = JOptionPane.showConfirmDialog(frame,"�Deseas guardar las modificaciones?","GUARDAR",JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
				if(value == JOptionPane.OK_OPTION ){
					
					LogForDay.writeToLog("Estableciendo conexion a BBDD ", QCS.logPath, LogForDay.DEBUG);
	        		Connection con = null;
					try {
						con = MainForm.getConexion(prop);
					} catch (SQLException err) {
						LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					} catch (ClassNotFoundException err) {
						LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					} catch (Exception err) {
						LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					}
					
					// CHECK SQL...
					
					String msg = "Error! Por favor, revisa la sentencia SQL y los valores de la tabla de parametros";
	        		
	        		String param	=	null;
	        		String valor	=	null;
	        		String type		=	null;
	        		String queryToVerify = areaSQL.getText();
	        		
	        		String[] listaNombresParams = new String[table.getRowCount()];
	        		
	        		for(int x=0; x<table.getRowCount();x++){
	        			listaNombresParams[x]	= (String)table.getValueAt(x, 1);
	        		}
	        		
	        		Arrays.sort(listaNombresParams, Collections.reverseOrder()); // Hay que ponerlo en orden de cadena mayor a menor
	        		
	        		for(int x=0; x<listaNombresParams.length;x++){
	        			
	        			for(int y=0; y<table.getRowCount();y++){
	        				
	        				param	= (String)table.getValueAt(y, 1);
	        				
	        				if( !param.equals(listaNombresParams[x]) ){
	        					continue;
	        				}
	        				
	            			type	= (String)table.getValueAt(y, 2);
	            			valor	= (String)table.getValueAt(y, 3);
	            			
	            			// Tipos STRING, se les aniade caracter '
	            			if(type.equals("STRING")){
	            				valor = "'".concat(valor).concat("'");
	            			}else if(type.equals("TIMESTAMP")){
	            				valor = " TO_DATE('" + valor + "','dd/mm/yyyy HH24:MI:SS') "; // value debe tener formato: dd/mm/yyyy HH24:MI:SS. Por ejemplo 28/07/2011 10:31:07
	            			}else if(type.equals("NUMBER")){
	            				// NADA QUE FORMATEAR
	            			}else if(type.equals("DECIMAL")){
	            				// NADA QUE FORMATEAR
	            			}else if(type.equals("DATE")){
	            				valor = " TO_DATE('" + valor + "','dd/mm/yyyy') "; // value debe tener formato: dd/mm/yyyy. Por ejemplo 28/07/2011 10:31:07
	            			}
	            			
	            			queryToVerify = queryToVerify.replaceAll(param, valor);
	            			break;
	            			
	        			}
	        			
	        		}
	        		
	        		// Se lanza la query formateada y si todo OK, entonces, se habilita el boton GUARDAR
	        		try {
						rs = Query.executeQuery(queryToVerify, prop);
					} catch (SQLException err) {
						
						LogForDay.writeToLog("Error al hacer rollback " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
						
						rs = null;
						msg = "Error de BBDD! " + err.getLocalizedMessage() ;
					} catch (ClassNotFoundException err) {
						LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					} catch (Exception err) {
						LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					}
	        		
	        		if(rs == null){
	        			botonGuardar.setEnabled(false);
	                    JOptionPane.showConfirmDialog(frame,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
	                    return;
	        		}
					
					LogForDay.writeToLog("SQL verificada correctamente", QCS.logPath, LogForDay.DEBUG);
					
					// QUERY
					String queryName 		= campoNombre.getText();
					String queryDesc 		= areaDesc.getText();
					String queryMaxRegister = campoMax.getText();
					
					try{
						Integer.parseInt(queryMaxRegister);
					}catch(NumberFormatException  err){
						
						LogForDay.writeToLog("Error al hacer rollback " + err.getLocalizedMessage(), QCS.logPath, LogForDay.DEBUG);
						
						try {
							con.rollback();
						} catch (SQLException err2) {
							LogForDay.writeToLog("Error al hacer rollback " + err2.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
						}
						msg = queryMaxRegister + "No es valido. Por favor introduce un n�mero como valor de registros m�ximo.";
    					JOptionPane.showConfirmDialog(frame,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
                    	return;
						
					}
					
					// FORMATEAR querySQL y cambiar los parametros por interrogaciones...
					String queryToSave = areaSQL.getText();
					String queryToSaveAux;
	        		// Se concatena un espacio en blanco al final de la query para que pueda buscar el ultimo parametro si este fuera el ultimo string de la sentencia
					queryToSave = queryToSave.concat(" ");
	        		
	        		param		= null;
	        		
	        		for(int x=0; x<table.getRowCount();x++){
	        			
	        			param		= (String)table.getValueAt(x, 1);
	        			System.out.println("Hay que sustituir el primer parametro:" + param + " por ?");
	            		
	            		queryToSaveAux = queryToSave;
	            		queryToSave = queryToSave.replaceAll(param, "?");
	            		
	            		if(queryToSaveAux.equals(queryToSave) ){
	            			try {
								con.rollback();
							} catch (SQLException err) {
								LogForDay.writeToLog("Error al hacer rollback " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
							}
	            			msg = "Cuidado! El parametro " + param + " no esta en la sentencia SQL";
	    					JOptionPane.showConfirmDialog(frame,msg,"Cuidado",JOptionPane.CLOSED_OPTION, JOptionPane.INFORMATION_MESSAGE);
	                    	//return;
	            		}
	            		
	        		}
	        		
					// Kitamos el ultimo espacio que hemos aniadido antes, porque si no... cada vez que guardamos hacemos la query mas larga :-)
	        		queryToSave = queryToSave.substring(0, queryToSave.length()-1 );

					try {
						
						queryToSaveAux = areaSQL.getText();
						int pos = 0;
						String rank;
						String numParam = null;
						String argMap = "";
						
						// Se sustituyen todos los parametros por RANK_#
						for(int x=0;x<table.getRowCount();x++){
							rank = String.valueOf(table.getValueAt(x, 0));
							queryToSaveAux = queryToSaveAux.replaceAll( (String)table.getValueAt(x, 1), "RANK_".concat( rank ).concat("#") );
						}
						
						// Mientras haya valores RANK_#
						while(true){
							
							pos = queryToSaveAux.indexOf("RANK_");
							if(pos == -1){
								break;
							}
							
							numParam = (String) queryToSaveAux.substring(pos+5, queryToSaveAux.indexOf("#", pos+4) ) ;
							
							// Si el valor RANK_# es igual a la row... se coge la fila que es el orden del parametro
							for(int x=0;x<table.getRowCount();x++){
								
								rank = String.valueOf(table.getValueAt(x, 0));
								
								if( numParam.equals(rank) ){
									argMap = argMap.concat( String.valueOf(x) ).concat(","); 
									queryToSaveAux = queryToSaveAux.replaceFirst("RANK_", "_");
									queryToSaveAux = queryToSaveAux.replaceFirst("#", "_");
									break;
								}
								
							}
							
						}
						
						// Quitar los caracteres enter, tabs, etc...
						queryToSave = queryToSave.trim();
						
						// Quitar la ultima coma
						if(argMap.length() > 0){
							argMap = argMap.substring(0, argMap.length()-1);
						}
						
						//ResultSet rs;
						
						if( mode == EditContainer.NEW){
							
							// Sentencia de actualizacion de query
							String insertQuery = "INSERT INTO GCQS_QUERY VALUES ( (SELECT MAX(QUERY_ID) + 1 FROM GCQS_QUERY), '" + queryName + "',NULL,NULL,'" + queryDesc + "',NULL,SYSDATE,0,NULL,NULL,NULL," + queryMaxRegister + ",0,'" + argMap + "','" + queryToSave + "')";
							
							if(Query.executeInsert(insertQuery, con) == 0){
								
								con.rollback();
								msg = "Hay algun problema en la sentencia o en el resto de campos";
			                	JOptionPane.showConfirmDialog(frame,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
			                	return;
								
							}
							
						}else{
							
							// Sentencia de actualizacion de query
							String updateQuery = "UPDATE GCQS_QUERY " +
							"SET QUERY_NAME = '" + queryName + "', " +
							"QUERY_DESCRIPTION = '" + queryDesc + "', " +
							"MAX_NUM_RET_REG = '" + queryMaxRegister + "', " +
							"ARGUMENTS_MAP = '" + argMap + "', " +
							"PREPARED_QUERY_LONG = '" + queryToSave + "' " +
							"WHERE QUERY_ID = " + queryID;
							
							int rowsUpdated = Query.executeUpdate(updateQuery, con);
							
							if(rowsUpdated == 0){
								con.rollback();
								msg = "Hay algun problema en la lista de parametros";
								JOptionPane.showConfirmDialog(frame,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
			                	return;
							}
							
						}
						
						String insertParameter = "";
						String updateParameter = "";
						
						// SE BORRAN TODOS SUS ARGUMENTOS
						int totalInserts;
						//int totalDeletes;
						
						if( mode == EditContainer.EDIT){
							String deletequeryArguments = "DELETE FROM GCQS_ARGUMENT WHERE QUERY_ID = " + queryID;
							Query.executeDelete(deletequeryArguments, con);
						}
						
						String fila;
						StringTokenizer	valores_lista;
						
						String nombreParam	= "";
						String tipoParam	= "";
						String idParam		= "";
						
						// SE INSERTAN TODOS LOS QUE HAYA EN LA TABLA...
						for(int cont=0; cont<vectorParamsIDs.size(); cont++){
							
							fila = (String)vectorParamsIDs.get(cont);
							valores_lista = new StringTokenizer(fila, EditContainer.SEPARADOR);
							
							nombreParam	= valores_lista.nextToken();
							tipoParam	= valores_lista.nextToken();
							idParam		= valores_lista.nextToken();
							
							insertParameter = "INSERT INTO GCQS_ARGUMENT (QUERY_ID, ARGUMENT_NAME, ARGUMENT_ORDER, ARGUMENT_TYPE, CREATE_DATE, UPDATE_DATE, ARGUMENT_ID, OPTIMIST_LOCK, UPDATE_PROGRAM, UPDATE_USER, ARGUMENT_DESCRIPTION, ARGUMENT_POSITION) VALUES (" +  queryID + ", TO_CHAR('" + nombreParam + "'), 0, '" + tipoParam + "', SYSDATE, NULL, DECODE ('" + idParam + "', '####',(SELECT MAX(ARGUMENT_ID)+1 FROM GCQS_ARGUMENT),'" + idParam + "'), NULL, NULL, NULL, NULL, NULL)";
							totalInserts = Query.executeInsert(insertParameter, con);
							
							if(totalInserts == 0){
								try {
									con.rollback();
								} catch (SQLException err) {
									LogForDay.writeToLog("Error al hacer rollback " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
								}
								con.rollback();
								msg = "Hay algun problema en la lista de parametros, con el parametro: " + nombreParam;
			                	JOptionPane.showConfirmDialog(frame,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
			                	return;
							}
							
						}
						
						String	SQLQuery = areaSQL.getText();
						String celda;
						int[] 		intArray	=	new int[table.getRowCount()];
						String[] 	stringArray	=	new String[table.getRowCount()];
						
						
						int orden = 1; // Simulando el Query System, habria que empezar por 2
						
						for( int parametro =0; parametro<table.getRowCount(); parametro++ ){
							celda	=	(String)table.getValueAt(parametro, 1);
							intArray[parametro]		= SQLQuery.indexOf(celda);
							stringArray[parametro]	= celda; 
						}
						
						// Se ordena de menor a mayor
						Arrays.sort(intArray);
						SQLQuery = SQLQuery.concat(" ");
						
						for( int parametro =0; parametro<intArray.length; parametro++ ){
							
							//nombreParametro = SQLQuery.substring(intArray[parametro],  SQLQuery.indexOf(" ",intArray[parametro]) );
							//nombreParametro = SQLQuery.substring(intArray[parametro],  intArray[parametro] + stringArray[parametro].length() );
							
							//System.out.println("Parametro: " + nombreParametro);
							
							// Actualizamos la fila de argumentos que tiene el nombre: nombreParametro y le ponemos el orden 1
							updateParameter = "UPDATE GCQS_ARGUMENT SET ARGUMENT_ORDER = " + orden + " WHERE QUERY_ID = " + queryID + " AND ARGUMENT_NAME = '" + stringArray[parametro] + "' ";
							int rowsUpdated = Query.executeUpdate(updateParameter, con);
							
							if(rowsUpdated == 0){
								con.rollback();
								msg = "Hay algun problema en la sentencia o en el resto de campos";
			                	JOptionPane.showConfirmDialog(frame,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
			                	return;
							}
							orden++;
						}
						
						con.commit();
						msg = "Los cambios se guardaron correctamente";
	                	JOptionPane.showConfirmDialog(frame,msg,"OK",JOptionPane.CLOSED_OPTION, JOptionPane.INFORMATION_MESSAGE);
	                	
	                	// Si todo es OK, se vuelve a la pantalla del menu
	                    frame.setContentPane(mainContenedor);
	                    frame.setVisible(true);
	                    
					} catch (SQLException err) {
						LogForDay.writeToLog("Error: " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
						try {
							con.rollback();
						} catch (SQLException err2) {
							LogForDay.writeToLog("Error al hacer rollback: " + err2.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
						}
						msg = "Hay algun problema en la sentencia o en el resto de campos";
	                	JOptionPane.showConfirmDialog(frame,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
					} catch (Exception err) {
						LogForDay.writeToLog("Error: " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
						try {
							con.rollback();
						} catch (SQLException err2) {
							LogForDay.writeToLog("Error al hacer rollback: " + err2.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
						}
						msg = "Hay algun problema en la sentencia o en el resto de campos";
	                	JOptionPane.showConfirmDialog(frame,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
					}
					
					try {
						MainForm.closeConexion(con);
						LogForDay.writeToLog("Conexxion BBDD cerrada", QCS.logPath, LogForDay.DEBUG);
					} catch (SQLException err) {
						LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					} catch (ClassNotFoundException err) {
						LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					}
					
        		}
        		
        	}
        });
        //panel.add(botonGuardar);
        //miContenedor.add(botonGuardar);

        // FILA 2
        /*******************************/
        etiqueta = new JLabel("DESCRIPCION: ");
        etiqueta.setBounds(20, 145, 100, 30); // x, y, ancho, alto
        miContenedor.add(etiqueta);
 
        areaDesc.setLineWrap(true);
        areaDesc.setWrapStyleWord(true);
        areaDesc.setEditable(true);
        areaDesc.setBounds(120, 150, 550, 100); // x, y, ancho, alto
        areaDesc.setText(sentenceDescription);
        areaDesc.setAutoscrolls(true);
        
	    // TODO Hay que poner que un escuchador de eventos controle que al meter un caracter no llegue al maximo... 
	    
	    JScrollPane sp = new JScrollPane(areaDesc);
	    sp.setBounds(120, 150, 550, 100); // x, y, ancho, alto
	    sp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
	    
	    //getContentPane().add(sp);
	    miContenedor.add(sp);
	    
        // BOTON
        botonVolver = new JButton();
        botonVolver.setText("VOLVER");
        botonVolver.setVerticalTextPosition(JButton.BOTTOM);
        botonVolver.setHorizontalTextPosition(JButton.CENTER);
        botonVolver.setIcon(iconBack);
        botonVolver.setBounds(430, 110, 102, 30); // x, y, ancho, alto
        botonVolver.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		int value = JOptionPane.showConfirmDialog(frame,"�Deseas volver sin guardar las modificaciones?","VOLVER",JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
        		
        		if(value == JOptionPane.OK_OPTION ){
        			frame.setContentPane(mainContenedor);
                    frame.setVisible(true);
        		}
        		
        	}
        });
        //miContenedor.add(botonVolver);
        
        // FILA 3
        /*******************************/
        
        // BOTON
        botonRestaurar = new JButton();
        botonRestaurar.setText("RESTAURAR");
        botonRestaurar.setBounds(430, 150, 102, 30); // x, y, ancho, alto
        botonRestaurar.setIcon(iconReset);
        botonRestaurar.setVerticalTextPosition(JButton.BOTTOM);
        botonRestaurar.setHorizontalTextPosition(JButton.CENTER);
        botonRestaurar.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		int value = JOptionPane.showConfirmDialog(frame,"�Deseas restaurar los cambios originales?","RESTAURAR",JOptionPane.YES_OPTION, JOptionPane.QUESTION_MESSAGE);
        		
        		if(value == JOptionPane.YES_OPTION ){
					// Restaurar todos los campos editables
        			// Nombre de la query
					campoNombre.setText(sentenceName);
        			// Descripcion de la query
					areaDesc.setText(sentenceDescription);
        			// Num max registros
					campoMax.setText(sentenceMaxRegisters);
        			
					// Parametros
					vectorParamsIDs.clear();
					rowData = new Object[listaArgumentos.size()][4];
			        
					while(model.getRowCount() > 0){
						model.removeRow(0);
					}
					
			        for(int x=0; x<listaArgumentos.size();x++){
			        	
			        	filaParametros = listaArgumentos.get(x);
			        	
			        	rowData[x][0] = filaParametros[0]; 
			        	rowData[x][1] = filaParametros[1];
			        	rowData[x][2] = filaParametros[2];
			        	
			        	
			        	//rowData[x][3] = "VALOR";
			        	if(filaParametros[2].equals("DATE")){
			    			dateFormat = new SimpleDateFormat(DATE_FORMAT_DATE);
			    	        date = new Date();
			    	        rowData[x][3] = dateFormat.format(date);
			    		}else if(filaParametros[2].equals("TIMESTAMP")){
			    			dateFormat = new SimpleDateFormat(DATE_FORMAT_TIMESTAMP);
			    	        date = new Date();
			    	        rowData[x][3] = dateFormat.format(date);
			    		}else{
			    			rowData[x][3] = "0";
			    		}
			        	
			        	model.addRow(rowData[x]);
			        	
			        	// Coger las colunas en un vector. Sintaxis: NOMBRE_ID_TIPO
			        	//vectorParamsIDs.add(filaParametros[3].concat("@UPDATE"));
			        	vectorParamsIDs.add(filaParametros[1] + EditContainer.SEPARADOR + filaParametros[2] + EditContainer.SEPARADOR + filaParametros[3]);
			        	
			        }
			        
        			// Consulta
        			areaSQL.setText(sentenceSQLFormatted);
        			
        			botonGuardar.setEnabled(true);
        		}
        		
        	}
        });
        
        //panel.add(botonRestaurar);
        //miContenedor.add(botonRestaurar);
        
        // MAX. REGISTROS 
        etiqueta = new JLabel("NUM. MAX. DE REGISTROS: ");
        etiqueta.setBounds(20, 260, 180, 12); // x, y, ancho, alto
        //panel.add(etiqueta);
        miContenedor.add(etiqueta);
        
        etiqueta = new JLabel("(0 = INFINITO)");
        etiqueta.setBounds(20, 272, 180, 10); // x, y, ancho, alto
        //panel.add(etiqueta);
        miContenedor.add(etiqueta);
        
        campoMax.setSize(50, 30);
        campoMax.setBounds(200, 260, 50, 30); // x, y, ancho, alto
        campoMax.setText(sentenceMaxRegisters); //
        campoMax.setEditable(true);
        campoMax.setVisible(true);
        campoMax.setColumns(4);
        //panel.add(campoMax);
        miContenedor.add(campoMax);
        
        // FILA 4
        /*******************************/
        etiqueta = new JLabel("PARAMETROS: ");
        etiqueta.setBounds(20, 290, 100, 30); // x, y, ancho, alto
        //panel.add(etiqueta);
        miContenedor.add(etiqueta);
        
        table.setBounds(120, 300, 500, 160); // x, y, ancho, alto
        
        JTableHeader header = table.getTableHeader();
        header.setBackground(Color.GREEN);
        
        sp = new JScrollPane(table);
	    sp.setBounds(120, 300, 500, 160); // x, y, ancho, alto
	    sp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
	    
	    //getContentPane().add(sp);
	    miContenedor.add(sp);
        
	    // Botones para editar la tabla de parametros
	    
	    ImageIcon icono = new ImageIcon("res/icons/Add-icon.png");
	    botonAddParam = new JButton();
	    botonAddParam.setIcon(icono);
	    botonAddParam.setBounds(640, 330, 30, 30); // x, y, ancho, alto
	    botonAddParam.setToolTipText("New Param.");
	    botonAddParam.addActionListener(new ActionListener() {

        	public void actionPerformed(ActionEvent event){
        		// Se abre un cuadro de dialogo y se pide el nombre y el tipo de parametro
        		ArgumentDialog customDialog = new ArgumentDialog(model, vectorParamsIDs, null, null, 0, ArgumentDialog.NEW_PARAMETER, botonGuardar);
        		
        		customDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        		customDialog.setLocationRelativeTo(frame);
    			customDialog.setVisible(true);
    			customDialog.setResizable(false);
        		
    			//botonGuardar.setEnabled(false);
        	}
        	
	    });
	    
	    //panel.add(botonAddParam);
	    miContenedor.add(botonAddParam);
	    
	    icono = new ImageIcon("res/icons/Edit-icon.png");
	    botonEditParam = new JButton();
	    botonEditParam.setIcon(icono);
	    botonEditParam.setBounds(640, 370, 30, 30); // x, y, ancho, alto
	    botonEditParam.setToolTipText("Edit Param.");
	    botonEditParam.addActionListener(new ActionListener() {

        	public void actionPerformed(ActionEvent event){
        		
        		int fila = table.getSelectedRow();
        		
        		if(fila > -1){
        			
        			String nombreParam	= (String)table.getValueAt(fila, 1);
        			String tipoParam	= (String)table.getValueAt(fila, 2);
        			
        			// Se abre un cuadro de dialogo y se rellena el nombre y el tipo de parametro
            		ArgumentDialog customDialog = new ArgumentDialog(model, vectorParamsIDs, nombreParam, tipoParam, fila, ArgumentDialog.EDIT_PARAMETER, botonGuardar);
            		
            		customDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
            		customDialog.setLocationRelativeTo(frame);
        			customDialog.setVisible(true);
        			customDialog.setResizable(false);
        			
        			//botonGuardar.setEnabled(false);
        			
        		}else{
        			String msg = "Selecciona una fila para editar un parametro";
					JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
        		}
        		
        	}
        	
	    });
	    
	    //panel.add(botonEditParam);
	    miContenedor.add(botonEditParam);
	    
	    icono = new ImageIcon("res/icons/Delete-icon.png");
	    botonRemoveParam = new JButton();
	    botonRemoveParam.setIcon(icono);
	    botonRemoveParam.setBounds(640, 410, 30, 30); // x, y, ancho, alto
	    botonRemoveParam.setToolTipText("Delete param.");
	    
	    botonRemoveParam.addActionListener(new ActionListener() {

        	public void actionPerformed(ActionEvent event){
        		
        		int fila = table.getSelectedRow();
        		
        		if(fila > -1){
            		// Se borra la fila seleccionada
            		model.removeRow(fila);
            		vectorParamsIDs.remove(fila);
            		botonGuardar.setEnabled(false);
        		}else{
        			String msg = "Selecciona una fila para borrar un parametro";
					JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
        		}
        		
        	}
        	
	    });
	    
	    miContenedor.add(botonRemoveParam);
	    
	    // FILA 5
        /*******************************/
	    etiqueta = new JLabel("CONSULTA: ");
        etiqueta.setBounds(20, 470, 100, 30); // x, y, ancho, alto
        miContenedor.add(etiqueta);
        
        // Area con la sentencia SQL
        
        areaSQL.setLineWrap(true);
	    areaSQL.setWrapStyleWord(true);
	    //area.setSize(300, 100);
	    areaSQL.setEditable(true);
	    areaSQL.setBounds(120, 480, 550, 220); // x, y, ancho, alto
	    areaSQL.setText(sentenceSQLFormatted);
	    areaSQL.setAutoscrolls(true);
	    
	    areaSQL.addKeyListener( new KeyListener() {
			
			public void keyTyped(KeyEvent e) {
				botonGuardar.setEnabled(false);
			}
			
			public void keyReleased(KeyEvent e) {
				
			}
			
			public void keyPressed(KeyEvent e) {
				
			}
		});
	    
	    sp = new JScrollPane(areaSQL);
	    sp.setBounds(120, 480, 550, 220); // x, y, ancho, alto
	    sp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	    
	    //getContentPane().add(sp);
	    miContenedor.add(sp);
        
	    botonVerify.setText("CHECK SQL");
        botonVerify.setBounds(10, 500, 100, 30); // x, y, ancho, alto
        botonVerify.setIcon(iconCheck);
        botonVerify.setVerticalTextPosition(JButton.BOTTOM);
        botonVerify.setHorizontalTextPosition(JButton.CENTER);
        botonVerify.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		LogForDay.writeToLog("Estableciendo conexion a BBDD ", QCS.logPath, LogForDay.DEBUG);
        		Connection con;
				try {
					con = MainForm.getConexion(prop);
				} catch (SQLException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					return;
				} catch (ClassNotFoundException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					return;
				} catch (Exception err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					return;
				}
        		
        		// TEST
        		TableColumn col;
                
                for(int z=0; z<table.getColumnCount(); z++){
                	col = table.getColumnModel().getColumn(z);
                	System.out.println("Columna: " + z + " -> " + col.getWidth());
                }
        		// TEST
        		
        		String msg;
        		
        		String param	=	null;
        		String value	=	null;
        		String type		=	null;
        		String queryToVerify = areaSQL.getText();
        		
        		// Cogemos todos los nombres de parametros para reemplazar luego desde el nombre mas largo al mas corto
        		
        		String[] listaNombresParams = new String[table.getRowCount()];
        		
        		for(int x=0; x<table.getRowCount();x++){
        			listaNombresParams[x]	= (String)table.getValueAt(x, 1);
        		}
        		
        		Arrays.sort(listaNombresParams, Collections.reverseOrder()); // Hay que ponerlo en orden de cadena mayor a menor
        		
        		for(int x=0; x<listaNombresParams.length;x++){
        			
        			for(int y=0; y<table.getRowCount();y++){
        				
        				param	= (String)table.getValueAt(y, 1);
        				
        				if( !param.equals(listaNombresParams[x]) ){
        					continue;
        				}
        				
            			type	= (String)table.getValueAt(y, 2);
            			value	= (String)table.getValueAt(y, 3);
            			
            			// Tipos STRING, se les aniade caracter '
            			if(type.equals("STRING")){
            				value = "'".concat(value).concat("'");
            			}else if(type.equals("TIMESTAMP")){
            				value = " TO_DATE('" + value + "','dd/mm/yyyy HH24:MI:SS') "; // value debe tener formato: dd/mm/yyyy HH24:MI:SS. Por ejemplo 28/07/2011 10:31:07
            			}else if(type.equals("NUMBER")){
            				// NADA QUE FORMATEAR
            			}else if(type.equals("DECIMAL")){
            				// NADA QUE FORMATEAR
            			}else if(type.equals("DATE")){
            				value = " TO_DATE('" + value + "','dd/mm/yyyy') "; // value debe tener formato: dd/mm/yyyy. Por ejemplo 28/07/2011 10:31:07
            			}
            			
            			queryToVerify = queryToVerify.replaceAll(param, value);
            			break;
            			
        			}
        			
        		}
        		
        		// Se lanza la query formateada y si todo OK, entonces, se habilita el boton GUARDAR
        		msg = "Error! Por favor, revisa la sentencia SQL y los valores de la tabla de parametros";
        		
        		try {
					rs = Query.executeQueryByConn(queryToVerify, con);
				} catch (SQLException err) {
					
					LogForDay.writeToLog(" de BBDD. " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					
					rs = null;
					msg = "Error de BBDD. " + err.getLocalizedMessage() ;
				} catch (ClassNotFoundException err) {
					LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
				} catch (Exception err) {
					LogForDay.writeToLog(" " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
				}
        		
        		if(rs!= null){

        			// Se muestra un popup con el resultado de la query
        			
            		ResultDialog resDialog;
					try {
						
						resDialog = new ResultDialog(frame, rs, 450, 400);
						resDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	            		resDialog.setLocationRelativeTo(frame);
	            		resDialog.setVisible(true);
	        			
	                    botonGuardar.setEnabled(true);
	                    
					} catch (Exception err) {
						LogForDay.writeToLog(" al verificar la sentencia " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					}
            		
        		}else{
        			botonGuardar.setEnabled(false);
        			JOptionPane.showConfirmDialog(frame,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
        		}
        		
        		try {
					MainForm.closeConexion(con);
					LogForDay.writeToLog("Conexxion BBDD cerrada", QCS.logPath, LogForDay.DEBUG);
				} catch (SQLException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
				} catch (ClassNotFoundException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
				}
				
				LogForDay.writeToLog("sentencia verificada correctamente ", QCS.logPath, LogForDay.ERROR);
        		
        	}
        });
        
        //panel.add(botonVerify);
        
        // Nuevos adds
        //miContenedor.add(botonVerify);
	    
        // TOOLBAR
        JToolBar toolbar = new JToolBar();
        toolbar.setSize(700, 75);
        toolbar.setFloatable(false);
        
        toolbar.add(botonGuardar);
        toolbar.addSeparator(); //SEPARADOR
        toolbar.add(botonVerify);
        toolbar.addSeparator(); //SEPARADOR
        toolbar.add(botonRestaurar);
        toolbar.addSeparator(); //SEPARADOR
        toolbar.add(botonVolver);
        
        miContenedor.add(toolbar);
        
        // Se a�ade el panel al formulario
        //add(panel);
        //miContenedor.add(panel);
        
	}
	
}
