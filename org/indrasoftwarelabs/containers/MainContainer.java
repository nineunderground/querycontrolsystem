package org.indrasoftwarelabs.containers;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.indrasoftwarelabs.db.Query;
import org.indrasoftwarelabs.db.Sentence;
import org.indrasoftwarelabs.log.LogForDay;
import org.indrasoftwarelabs.main.MainForm;
import org.indrasoftwarelabs.main.QCS;

@SuppressWarnings("serial")
public class MainContainer extends Container {
	
	Container miContenedor = new Container();
	
	public JList	listMod;
	public JList	listMet;
	public JList	listSen;
	
	JLabel	labelMod;
	JLabel	labelMet;
	JLabel	labelSen;
	
	JPanel panel = new JPanel();
	JPanel panel2 = new JPanel();
	JPanel panel3 = new JPanel();
	
	public ResultSet rs;
	public String[] modules;
	public String[] methods = new String[0];
	public String[] queries = new String[0];
	
	// Frame components
	JScrollPane pane2;
	
	public MainContainer(final JFrame frame, final Properties prop) throws SQLException, ClassNotFoundException, Exception{
		
//        ImageIcon iconNew		=	null;
//        ImageIcon iconEdit	=	null;
//        ImageIcon iconDelete	=	null;
//        ImageIcon iconAbout	=	null;
        
		ImageIcon iconSearch	=	null;
        ImageIcon iconExit		=	null;
        ImageIcon iconTitulo	=	null;
        ImageIcon iconOrganize	=	null;
        ImageIcon iconAdd		=	null;
        ImageIcon iconDelete	=	null;
        ImageIcon iconEdit		=	null;
        
        Image img;
        
		try {
			img = ImageIO.read(new File("res/icon.png"));
			frame.setIconImage(img);	
		} catch (IOException err) {
			LogForDay.writeToLog("No se encuentra la ruta res/icon.png " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
		}
		
		try {
			img = ImageIO.read(new File("res/QCS_titulo.png"));
			iconTitulo = new ImageIcon(img);	
		} catch (IOException err) {
			LogForDay.writeToLog("No se encuentra la ruta res/QCS_titulo.png " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
		}
		
		try {
			img = ImageIO.read(new File("res/icons/Refresh-icon.png"));
			iconOrganize = new ImageIcon(img);	
		} catch (IOException err) {
			LogForDay.writeToLog("No se encuentra la ruta res/icons/Refresh-icon.png " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
		}
		
		try {
			img = ImageIO.read(new File("res/icons/Search-icon.png"));
			iconSearch = new ImageIcon(img);	
		} catch (IOException err) {
			LogForDay.writeToLog("No se encuentra la ruta res/icons/Search-icon.png " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
		}
		
		try {
			img = ImageIO.read(new File("res/icons/Login-out-icon.png"));
			iconExit = new ImageIcon(img);	
		} catch (IOException err) {
			LogForDay.writeToLog("No se encuentra la ruta res/icons/Login-out-icon.png " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
		}
		
		try {
			img = ImageIO.read(new File("res/icons/Add-icon.png"));
			iconAdd = new ImageIcon(img);	
		} catch (IOException err) {
			LogForDay.writeToLog("No se encuentra la ruta res/icons/Add-icon.png " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
		}
		
		try {
			img = ImageIO.read(new File("res/icons/Delete-icon.png"));
			iconDelete = new ImageIcon(img);	
		} catch (IOException err) {
			LogForDay.writeToLog("No se encuentra la ruta res/icons/Delete-icon.png " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
		}
		
		try {
			img = ImageIO.read(new File("res/icons/Edit-icon.png"));
			iconEdit = new ImageIcon(img);	
		} catch (IOException err) {
			LogForDay.writeToLog("No se encuentra la ruta res/icons/Edit-icon.png " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
		}
		
		frame.setLayout(null);
        
        // BOTONES EN TOOLBAR
        // ORGANIZE - SEARCH - EXIT
        
		JButton botonOrganize = new JButton();
		botonOrganize.setText("ORGANIZE");
        botonOrganize.setVerticalTextPosition(JButton.BOTTOM);
        botonOrganize.setHorizontalTextPosition(JButton.CENTER);
        botonOrganize.setIcon(iconOrganize);
        botonOrganize.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		OrganizeContainer mainOrganize = new OrganizeContainer(frame, prop, miContenedor, listMod, listMet, listSen);
        		frame.setContentPane(mainOrganize.getMyContainer());
        		frame.setVisible(true);
        		
        	}
        });
		
		JButton botonSearch = new JButton();
		botonSearch.setText("SEARCH");
        botonSearch.setVerticalTextPosition(JButton.BOTTOM);
        botonSearch.setHorizontalTextPosition(JButton.CENTER);
        botonSearch.setIcon(iconSearch);
        botonSearch.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		SearchContainer mainSearch = new SearchContainer(frame, prop, miContenedor);
        		frame.setContentPane(mainSearch.getMyContainer());
        		frame.setVisible(true);
        		
        	}
        });
		
        JButton botonQuit = new JButton();
        botonQuit.setText("QUIT");
        botonQuit.setVerticalTextPosition(JButton.BOTTOM);
        botonQuit.setHorizontalTextPosition(JButton.CENTER);
        botonQuit.setIcon(iconExit);
        botonQuit.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		int value = JOptionPane.showConfirmDialog(frame,"¿Quieres salir de la aplicacion?","SALIR",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        		if(value == JOptionPane.YES_OPTION ){
        			LogForDay.writeToLog("Aplicacion cerrada" , QCS.logPath, LogForDay.ERROR);
					System.exit(0);
        		}
        		
        	}
        });
        
        // TOOLBAR
        JToolBar toolbar = new JToolBar();
        toolbar.setSize(700, 75);
        toolbar.setFloatable(false);
        
        toolbar.add(botonOrganize);
        toolbar.addSeparator(); //SEPARADOR
        toolbar.add(botonSearch);
        toolbar.addSeparator(); //SEPARADOR
        toolbar.add(botonQuit);
        
        miContenedor.add(toolbar);
        
        // ETIQUETAS
        
        labelMod = new JLabel("MODULES");
        labelMod.setFont(new Font("Serif", Font.BOLD, 12));
        labelMod.setBounds(20, 60, 90, 80);
        miContenedor.add(labelMod);
        
        labelMet = new JLabel("METHODS");
        labelMet.setFont(new Font("Serif", Font.BOLD, 12));
        labelMet.setBounds(340, 60, 90, 80);
        miContenedor.add(labelMet);
        
        labelSen = new JLabel("SENTENCES");
        labelSen.setFont(new Font("Serif", Font.BOLD, 12));
        labelSen.setBounds(20, 350, 90, 80);
        miContenedor.add(labelSen);
        
        // LISTA DE LOS MODULOS
        
        LogForDay.writeToLog("Estableciendo conexion a BBDD ", QCS.logPath, LogForDay.DEBUG);
		//Connection con = MainForm.getConexion(prop);
        Connection con = Query.getConexionThread(prop); //TESTING
        
        rs = Query.executeSelectByConn(Sentence.GET_ALL_MODULES, con);
        
        try {
			rs.last();
			modules = new String[rs.getRow()];
			rs.beforeFirst();
		} catch (SQLException err) {
			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
		}
        
    	try {
    		int cont = 0;
			while (rs.next()){
				String name =	rs.getString("MODULE_NAME");
				modules[cont] = name;
				cont++;
			}
			
			rs.close();
			
			MainForm.closeConexion(con);
			LogForDay.writeToLog("Conexxion BBDD cerrada", QCS.logPath, LogForDay.DEBUG);
			
		} catch (SQLException err) {
			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
		}
        
        listMod = new JList(modules);
        listMod.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listMod.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {

                	LogForDay.writeToLog("Estableciendo conexion a BBDD ", QCS.logPath, LogForDay.DEBUG);
                	Connection con = null;
            		try {
						con = MainForm.getConexion(prop);
					} catch (SQLException err) {
						LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
						return;
					} catch (ClassNotFoundException err) {
						LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
						return;
					} catch (Exception err) {
						LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
						return;
					}
            		
                	// Ejecutar la query y poner los valores a listMet
                	String nameModule = (String) listMod.getSelectedValue();
                	String[][] params = Query.sentenceAddParam(Query.TYPE_STRING, nameModule, 1);
                	
                    try {
                    	
                    	if(nameModule != null && nameModule.equals(" --ALL--")){
                    		params[0][1] =  "%";
                    		rs = Query.executeQueryWithParamsByConn(Sentence.GET_TOTAL_METHODS_WITH_ALL, params, con);
                    	}else{
                    		rs = Query.executeQueryWithParamsByConn(Sentence.GET_TOTAL_METHODS_BY_MODULE_ID, params, con);
                    	}
                    	
                    	rs.next();
                    	methods	= new String[rs.getInt("TOTAL")];
                    	
                    	if(nameModule != null && nameModule.equals(" --ALL--")){
                    		rs = Query.executeQueryByConn(Sentence.GET_ALL_METHODS_WITH_ALL, con);
                    	}else{
                    		rs = Query.executeQueryWithParamsByConn(Sentence.GET_METHODS_BY_MODULE_ID, params, con);
                    	}
                    	
                		int cont = 0;
                		
            			while (rs.next()){
            				String name =	rs.getString("METHOD_NAME");
            				methods[cont] = name;
            				cont++;
            			}
            			
            			rs.close();
            			Query.stmt.close();
            			
            		} catch (SQLException err) {
            			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
            			methods	= new String[0];
            		} catch (Exception err) {
            			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
					}
            		
            		// REFRESH VALUES
                    listMet.setListData(methods);
                    
                    try {
    					MainForm.closeConexion(con);
    					LogForDay.writeToLog("Conexxion BBDD cerrada", QCS.logPath, LogForDay.DEBUG);
    				} catch (SQLException err) {
    					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
    				} catch (ClassNotFoundException err) {
    					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
    				}
                    
                }
            }
        });
        
        JScrollPane pane = new JScrollPane();
        pane.getViewport().add(listMod);
        
        pane.setPreferredSize(new Dimension(300,180));
        
        panel.add(pane);
        panel.setBounds(20, 110, 300, 180);
        miContenedor.add(panel);
        
        // LIST DE LOS METODOS
        
        listMet = new JList(methods);
        listMet.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listMet.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
            	
            	if (!e.getValueIsAdjusting()) {

            		LogForDay.writeToLog("Estableciendo conexion a BBDD ", QCS.logPath, LogForDay.DEBUG);
                	Connection con = null;
            		try {
						con = MainForm.getConexion(prop);
					} catch (SQLException err) {
						LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
						return;
					} catch (ClassNotFoundException err) {
						LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
						return;
					} catch (Exception err) {
						LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
						return;
					}
            		
                	// Ejecutar la query y poner los valores a listMet
                	String nameMethod = (String) listMet.getSelectedValue();
                	String[][] params = Query.sentenceAddParam(Query.TYPE_STRING, nameMethod, 1);
                	
                    try {
                    	
                    	if(nameMethod != null && nameMethod.equals(" --ALL--")){
                    		params[0][1] =  "%";
                    		rs = Query.executeQueryWithParamsByConn(Sentence.GET_TOTAL_QUERIES_WITH_ALL, params, con);
                    	}else{
                    		rs = Query.executeQueryWithParamsByConn(Sentence.GET_TOTAL_QUERIES_BY_METHOD_NAME, params, con);
                    	}
                    	
                    	rs.next();
                    	queries	= new String[rs.getInt("TOTAL")];
                    	
                    	if(nameMethod != null && nameMethod.equals(" --ALL--")){
                    		rs = Query.executeQueryByConn(Sentence.GET_ALL_QUERIES_WITH_ALL, con);
                    	}else{
                    		rs = Query.executeQueryWithParamsByConn(Sentence.GET_QUERY_BY_METHOD_NAME, params, con);
                    	}
                    	
                    	int cont = 0;
                		
            			while (rs.next()){
            				String name =	rs.getString("QUERY_NAME");
            				queries[cont] = name;
            				cont++;
            			}
            			
            			rs.close();
            			Query.stmt.close();
            			
            		} catch (SQLException err) {
            			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
            			queries	= new String[0];
            		} catch (Exception err) {
            			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
					}
            		
            		// REFRESH VALUES
            		listSen.setListData(queries);
            		
            		try {
    					MainForm.closeConexion(con);
    					LogForDay.writeToLog("Conexxion BBDD cerrada", QCS.logPath, LogForDay.DEBUG);
    				} catch (SQLException err) {
    					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
    				} catch (ClassNotFoundException err) {
    					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
    				}
            		
                }
            	
            }
        });
        
        pane2 = new JScrollPane();
        pane2.getViewport().add(listMet);
        pane2.setPreferredSize(new Dimension(300,180));
        
        panel2.add(pane2);
        panel2.setBounds(340, 110, 300, 180);
        
        miContenedor.add(panel2);
        
        // Botones add, edit & quit MODULOS
        JButton botonAgregarMod;
        JButton botonEditarMod;
        JButton botonBorrarMod;
        
        botonAgregarMod = new JButton();
        botonAgregarMod.setBounds(20, 300, 30, 30); // x, y, ancho, alto
        botonAgregarMod.setIcon(iconAdd);
        botonAgregarMod.setToolTipText("NEW");
        botonAgregarMod.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		final ModuleMethodContainer moduleNew = new ModuleMethodContainer(ModuleMethodContainer.MODULO, true, frame, prop, miContenedor, null, listMod, listMet, listSen);
        		frame.setContentPane(moduleNew.getMyContainer());
        		frame.setVisible(true);
        	}
        });
        miContenedor.add(botonAgregarMod);
        
        botonEditarMod = new JButton();
        botonEditarMod.setBounds(60, 300, 30, 30); // x, y, ancho, alto
        botonEditarMod.setIcon(iconEdit);
        botonEditarMod.setToolTipText("EDIT");
        botonEditarMod.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		// Editar modulo...
        		// Si no hay una query seleccionada, sacar un mensaje de dialogo advirtiendo del uso correcto
        		String valor = (String)listMod.getSelectedValue();
        		
        		if(valor == null){
        			String msg = "Por favor, selecciona un modulo de la lista";
            		JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
                	
        		}else{
        			final ModuleMethodContainer moduleNew = new ModuleMethodContainer(ModuleMethodContainer.MODULO, false, frame, prop, miContenedor, valor, listMod, listMet, listSen);
            		frame.setContentPane(moduleNew.getMyContainer());
            		frame.setVisible(true);
        		}
        		
        	}
        });
        miContenedor.add(botonEditarMod);
        
        botonBorrarMod = new JButton();
        botonBorrarMod.setBounds(100, 300, 30, 30); // x, y, ancho, alto
        botonBorrarMod.setIcon(iconDelete);
        botonBorrarMod.setToolTipText("DELETE");
        botonBorrarMod.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		LogForDay.writeToLog("Estableciendo conexion a BBDD ", QCS.logPath, LogForDay.DEBUG);
        		Connection con = null;
				try {
					con = MainForm.getConexion(prop);
				} catch (SQLException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					return;
				} catch (ClassNotFoundException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					return;
				} catch (Exception err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
					return;
				}
        		
        		String valor = (String)listMod.getSelectedValue();
        		
        		if(valor == null || valor.equals(" --ALL--")){
        			String msg = "Por favor, selecciona una modulo de la lista";
            		JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
        		}else{
        			
	        		int value = JOptionPane.showConfirmDialog(frame,"¿Deseas eliminar ese modulo?","BORRAR",JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
	        		
	        		if(value == JOptionPane.OK_OPTION ){
	        			
	        			String deletequeryArguments = "DELETE FROM GCQS_MODULE WHERE MODULE_NAME = '" + valor + "'";
						int deleteRows = 0;
						try {
							deleteRows = Query.executeDelete(deletequeryArguments, con);
						} catch (SQLException err) {
							LogForDay.writeToLog("Error: " + deletequeryArguments + " - " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
						} catch (ClassNotFoundException err) {
							LogForDay.writeToLog("Error: " + deletequeryArguments + " - " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
						} catch (Exception err) {
							LogForDay.writeToLog("Error: " + deletequeryArguments + " - " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
						}
						
						if(deleteRows == 0){
							String msg = "Hay un error al eliminar el modulo. Por favor revisa la BBDD.";
		            		JOptionPane.showConfirmDialog(frame,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
						}else{
							String msg = "Modulo eliminado correctamente.";
		            		JOptionPane.showConfirmDialog(frame,msg,"OK",JOptionPane.CLOSED_OPTION, JOptionPane.INFORMATION_MESSAGE);
						}
						
						// Refrescar los modulos
						// LISTA DE LOS MODULOS
				        try {
				        	rs = Query.executeSelectByConn(Sentence.GET_ALL_MODULES, con);
							rs.last();
							modules = new String[rs.getRow()];
							rs.beforeFirst();
						} catch (SQLException err) {
							LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
						} catch (ClassNotFoundException err) {
							LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
						} catch (Exception err) {
							LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
						}
				        
				    	try {
				    		int cont = 0;
							while (rs.next()){
								//int	id		=	rs.getInt("MODULE_ID");
								String name =	rs.getString("MODULE_NAME");
								modules[cont] = name;
								cont++;
							}
							
							rs.close();
							
						} catch (SQLException err) {
							LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
						}
	        			
						listMod.setListData(modules);
						
	        		}
	        		
        		}
        		
        		try {
					MainForm.closeConexion(con);
					LogForDay.writeToLog("Conexxion BBDD cerrada", QCS.logPath, LogForDay.DEBUG);
				} catch (SQLException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
				} catch (ClassNotFoundException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
				}
        		
        	}
        });
        miContenedor.add(botonBorrarMod);
        
        // Botones add, edit & quit METODOS
        JButton botonAgregarMet;
        JButton botonEditarMet;
        JButton botonBorrarMet;
        
        botonAgregarMet = new JButton();
        botonAgregarMet.setBounds(340, 300, 30, 30); // x, y, ancho, alto
        botonAgregarMet.setIcon(iconAdd);
        botonAgregarMet.setToolTipText("NEW");
        botonAgregarMet.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		final ModuleMethodContainer moduleNew = new ModuleMethodContainer(ModuleMethodContainer.METODO, true, frame, prop, miContenedor, null, listMod, listMet, listSen);
        		frame.setContentPane(moduleNew.getMyContainer());
        		frame.setVisible(true);
        	}
        });
        miContenedor.add(botonAgregarMet);
        
        botonEditarMet = new JButton();
        botonEditarMet.setBounds(380, 300, 30, 30); // x, y, ancho, alto
        botonEditarMet.setIcon(iconEdit);
        botonEditarMet.setToolTipText("EDIT");
        botonEditarMet.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		// Si no hay una query seleccionada, sacar un mensaje de dialogo advirtiendo del uso correcto
        		String valor = (String)listMet.getSelectedValue();
        		
        		if(valor == null){
        			String msg = "Por favor, selecciona un metodo de la lista";
            		JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
                	
        		}else{
        			final ModuleMethodContainer moduleNew = new ModuleMethodContainer(ModuleMethodContainer.METODO, false, frame, prop, miContenedor, valor, listMod, listMet, listSen);
            		frame.setContentPane(moduleNew.getMyContainer());
            		frame.setVisible(true);
        		}
        		
        	}
        });
        miContenedor.add(botonEditarMet);
        
        botonBorrarMet = new JButton();
        botonBorrarMet.setBounds(420, 300, 30, 30); // x, y, ancho, alto
        botonBorrarMet.setIcon(iconDelete);
        botonBorrarMet.setToolTipText("DELETE");
        botonBorrarMet.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		// Si no hay una query seleccionada, sacar un mensaje de dialogo advirtiendo del uso correcto
        		String valor = (String)listMet.getSelectedValue();
        		
        		if(valor == null || valor.equals(" --ALL--")){
        			String msg = "Por favor, selecciona una metodo de la lista";
            		JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
        		}else{
        			
        			int value = JOptionPane.showConfirmDialog(frame,"¿Deseas eliminar ese metodo?","BORRAR",JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
            		
            		if(value == JOptionPane.OK_OPTION ){
            			
            			LogForDay.writeToLog("Estableciendo conexion a BBDD ", QCS.logPath, LogForDay.DEBUG);
                		Connection con = null;
        				try {
        					con = MainForm.getConexion(prop);
        				} catch (SQLException err) {
        					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
        					return;
        				} catch (ClassNotFoundException err) {
        					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
        					return;
        				} catch (Exception err) {
        					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
        					return;
        				}
            			
            			// Se borra la query...
            			String deletequeryArguments = "DELETE FROM GCQS_METHOD WHERE METHOD_NAME = '" + valor + "'";
						int deleteRows = 0;
						try {
							deleteRows = Query.executeDelete(deletequeryArguments, con);
						} catch (SQLException err) {
							LogForDay.writeToLog(" " + deletequeryArguments + " - " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
						} catch (ClassNotFoundException err) {
							LogForDay.writeToLog(" " + deletequeryArguments + " - " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
						} catch (Exception err) {
							LogForDay.writeToLog(" " + deletequeryArguments + " - " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
						}
						
						if(deleteRows == 0){
							String msg = "Hay un error al eliminar el metodo. Por favor revisa la BBDD.";
		            		JOptionPane.showConfirmDialog(frame,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
						}else{
							String msg = "Metodo eliminado correctamente.";
		            		JOptionPane.showConfirmDialog(frame,msg,"OK",JOptionPane.CLOSED_OPTION, JOptionPane.INFORMATION_MESSAGE);
						}
						
						try {
							con.commit();
						} catch (SQLException err) {
							LogForDay.writeToLog(" commit failed. " + deletequeryArguments + " - " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
						}
						
						// Refrescar los metodos
						// Ejecutar la query y poner los valores a listMet
	                	String nameModule = (String) listMod.getSelectedValue();
	                	String[][] params = Query.sentenceAddParam(Query.TYPE_STRING, nameModule, 1);
	                	
	                    try {
	                    	
	                    	rs = Query.executeQueryWithParamsByConn(Sentence.GET_TOTAL_METHODS_BY_MODULE_ID, params, con);
	                    	rs.next();
	                    	methods	= new String[rs.getInt("TOTAL")];
	                    	
	                    	rs = Query.executeQueryWithParamsByConn(Sentence.GET_METHODS_BY_MODULE_ID, params, con);
	                		int cont = 0;
	                		
	            			while (rs.next()){
	            				String name =	rs.getString("METHOD_NAME");
	            				methods[cont] = name;
	            				cont++;
	            			}
	            			
	            			rs.close();
	            			Query.stmt.close();
	            			
	            		} catch (SQLException err) {
	            			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
	            			methods	= new String[0];
	            		} catch (Exception err) {
	            			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
						}
	            		
	            		// REFRESH VALUES
	                    listMet.setListData(methods);
						
						// Refrescar las queries...
						
						// Ejecutar la query y poner los valores a listMet
	                	String nameMethod = (String) listMet.getSelectedValue();
	                	params = Query.sentenceAddParam(Query.TYPE_STRING, nameMethod, 1);
	                	
	                    try {
	                    	
	                    	rs = Query.executeQueryWithParamsByConn(Sentence.GET_TOTAL_QUERIES_BY_METHOD_NAME, params, con);
	                    	rs.next();
	                    	queries	= new String[rs.getInt("TOTAL")];
	                    	
	                    	rs = Query.executeQueryWithParamsByConn(Sentence.GET_QUERY_BY_METHOD_NAME, params, con);
	                		int cont = 0;
	                		
	            			while (rs.next()){
	            				String name =	rs.getString("QUERY_NAME");
	            				queries[cont] = name;
	            				cont++;
	            			}
	            			
	            			rs.close();
	            			Query.stmt.close();
	            			
	            		} catch (SQLException err) {
	            			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
	            			queries	= new String[0];
	            		} catch (Exception err) {
	            			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
						}
	            		
	            		// REFRESH VALUES
	            		listSen.setListData(queries);
						
	            		try {
	    					MainForm.closeConexion(con);
	    					LogForDay.writeToLog("Conexxion BBDD cerrada", QCS.logPath, LogForDay.DEBUG);
	    				} catch (SQLException err) {
	    					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
	    				} catch (ClassNotFoundException err) {
	    					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
	    				}
	            		
            		}
        			
        		}
        		
        		
        	}
        });
        miContenedor.add(botonBorrarMet);
        
        // LISTA DE LAS CONSULTAS
        
        listSen = new JList(queries);
        listSen.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        JScrollPane pane3 = new JScrollPane();
        pane3.getViewport().add(listSen);
        pane3.setPreferredSize(new Dimension(620,150));
        
        panel3.add(pane3);
        panel3.setBounds(20, 400, 620, 150);

        miContenedor.add(panel3);
        
        // Botones add, edit & quit SENTENCIAS
        JButton botonAgregarSen;
        JButton botonEditarSen;
        JButton botonBorrarSen;
        
        botonAgregarSen = new JButton();
        botonAgregarSen.setBounds(20, 560, 30, 30); // x, y, ancho, alto
        botonAgregarSen.setIcon(iconAdd);
        botonAgregarSen.setToolTipText("NEW");
        botonAgregarSen.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		EditContainer mainNew = new EditContainer(null, frame, prop, miContenedor);
        		frame.setContentPane(mainNew.getMyContainer());
        		frame.setVisible(true);
        	}
        });
        miContenedor.add(botonAgregarSen);
        
        botonEditarSen = new JButton();
        botonEditarSen.setBounds(60, 560, 30, 30); // x, y, ancho, alto
        botonEditarSen.setIcon(iconEdit);
        botonEditarSen.setToolTipText("EDIT");
        botonEditarSen.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		// Si no hay una query seleccionada, sacar un mensaje de dialogo advirtiendo del uso correcto
        		String valor = (String)listSen.getSelectedValue();
        		
        		if(valor == null){
        			String msg = "Por favor, selecciona una sentencia de la lista";
            		JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
        		}else{
        			EditContainer mainNew = new EditContainer(valor, frame, prop, miContenedor);
            		frame.setContentPane(mainNew.getMyContainer());
            		frame.setVisible(true);
        		}
        		
        	}
        });
        miContenedor.add(botonEditarSen);
        
        botonBorrarSen = new JButton();
        botonBorrarSen.setBounds(100, 560, 30, 30); // x, y, ancho, alto
        botonBorrarSen.setIcon(iconDelete);
        botonBorrarSen.setToolTipText("DELETE");
        botonBorrarSen.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		// Si no hay una query seleccionada, sacar un mensaje de dialogo advirtiendo del uso correcto
        		String valor = (String)listSen.getSelectedValue();
        		
        		if(valor == null){
        			String msg = "Por favor, selecciona una sentencia de la lista";
            		JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
        		}else{
        			
        			int value = JOptionPane.showConfirmDialog(frame,"¿Deseas eliminar esa sentencia?","BORRAR",JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
            		
            		if(value == JOptionPane.OK_OPTION ){
            			
            			LogForDay.writeToLog("Estableciendo conexion a BBDD ", QCS.logPath, LogForDay.DEBUG);
                		Connection con = null;
        				try {
        					con = MainForm.getConexion(prop);
        				} catch (SQLException err) {
        					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
        					return;
        				} catch (ClassNotFoundException err) {
        					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
        					return;
        				} catch (Exception err) {
        					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
        					return;
        				}
            			
            			// Se borra la query...
            			String deletequeryArguments = "DELETE FROM GCQS_QUERY WHERE QUERY_NAME = '" + valor + "'";
						int deleteRows = 0;
						try {
							deleteRows = Query.executeDelete(deletequeryArguments, con);
						} catch (SQLException err) {
							LogForDay.writeToLog("Error: " + deletequeryArguments + " - " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
						} catch (ClassNotFoundException err) {
							LogForDay.writeToLog("Error: " + deletequeryArguments + " - " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
						} catch (Exception err) {
							LogForDay.writeToLog("Error: " + deletequeryArguments + " - " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
						}
						
						if(deleteRows == 0){
							String msg = "Hay un error al eliminar la sentencia. Por favor revisa la BBDD.";
		            		JOptionPane.showConfirmDialog(frame,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
						}
						
						try {
							con.commit();
						} catch (SQLException err) {
							LogForDay.writeToLog("Error: Commit failed when " + deletequeryArguments + " - " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
						}
						
						// Refrescar sus queries...
						// TEST
						
						// Ejecutar la query y poner los valores a listMet
	                	String nameMethod = (String) listMet.getSelectedValue();
	                	String[][] params = Query.sentenceAddParam(Query.TYPE_STRING, nameMethod, 1);
	                	
	                    try {
	                    	
	                    	rs = Query.executeQueryWithParams(Sentence.GET_TOTAL_QUERIES_BY_METHOD_NAME, params, prop);
	                    	rs.next();
	                    	queries	= new String[rs.getInt("TOTAL")];
	                    	
	                    	rs = Query.executeQueryWithParams(Sentence.GET_QUERY_BY_METHOD_NAME, params, prop);
	                		int cont = 0;
	                		
	            			while (rs.next()){
	            				String name =	rs.getString("QUERY_NAME");
	            				queries[cont] = name;
	            				cont++;
	            			}
	            			
	            			rs.close();
	            			Query.stmt.close();
	            			
	            		} catch (SQLException err) {
	            			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
	            			queries	= new String[0];
	            		} catch (Exception err) {
	            			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
						}
	            		
	            		// REFRESH VALUES
	            		listSen.setListData(queries);
						
	            		try {
	    					MainForm.closeConexion(con);
	    					LogForDay.writeToLog("Conexxion BBDD cerrada", QCS.logPath, LogForDay.DEBUG);
	    				} catch (SQLException err) {
	    					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
	    				} catch (ClassNotFoundException err) {
	    					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
	    				}
	            		
            		}
        			
        		}
        		
        	}
        });
        miContenedor.add(botonBorrarSen);
		
        JLabel label = new JLabel();
        label.setBounds(20, 640, 620, 140); // x, y, ancho, alto
        label.setIcon(iconTitulo);
        
        miContenedor.add(label);
        
	}
	
	/**
	 * Get Container
	 * @return
	 */
	public Container getMyContainer() {
		return miContenedor;
	}

}
