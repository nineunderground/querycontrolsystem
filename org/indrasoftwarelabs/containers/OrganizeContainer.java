package org.indrasoftwarelabs.containers;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import org.indrasoftwarelabs.db.Query;
import org.indrasoftwarelabs.db.Sentence;
import org.indrasoftwarelabs.dialogs.AgregarDialog;
import org.indrasoftwarelabs.log.LogForDay;
import org.indrasoftwarelabs.main.MainForm;
import org.indrasoftwarelabs.main.QCS;

@SuppressWarnings("serial")
public class OrganizeContainer extends Container {
	
	Container miContenedor = new Container();
	
	ResultSet rs;
	
	String sentenceName;
	String sentenceDescription;
	String sentenceSQL;
	String sentenceSQLFormatted;
	String sentenceArgsMap;
	String sentenceMaxRegisters;
	
	int		valueOrder;
	String	queryNAME;
	int		queryID;
	
	final DefaultTableModel model = new DefaultTableModel();
	
	//Connection con;
	
	List<String[]> listaArgumentos = new ArrayList<String[]>();
	String[] filaParametros = null;
	
	final List<String> vectorParamsIDs = new ArrayList<String>();
	
	String columnNames[] = {"#", "MODULO", "METODO", "SENTENCIA"};
	Object[] rowData = null;
	
	static final String SEPARADOR = "?*?";

	final String DATE_FORMAT_DATE		= "dd/MM/yyyy";
	final String DATE_FORMAT_TIMESTAMP	= "dd/MM/yyyy HH:mm:ss";
	DateFormat dateFormat;
	Date date;
	
	String[] modules;
	String[] methods;
	String[] sentences;
	
	private	JList	listMet;
	JList	listMod_Sen;
	
	JTextField	campoModulo;
	
	private JScrollPane scroll;
	private JPanel panelScroll;
	
	/**
	 * Get Container
	 * @return
	 */
	public Container getMyContainer() {
		return miContenedor;
	}
	
	public OrganizeContainer(final JFrame frame, final Properties prop, final Container mainContenedor, final JList listModPrev, final JList listMetPrev, final JList listSenPrev){
		
		//con = conn;
    	int cont = 0;
    	
    	try{
	    	Image img = ImageIO.read(new File("res/icon.png"));
			frame.setIconImage(img);
    	}catch(IOException err){
    		LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
    	}
    	
    	try {
    		
    		// CARGANDO TODOS LOS METODOS
    		rs = Query.executeSelect(Sentence.GET_ALL_METHODS, prop);
    		
    		rs.last();
    		methods = new String[rs.getRow()];
    		rs.beforeFirst();
    		
    		cont = 0;
    		while (rs.next()){
    			String name =	rs.getString("METHOD_NAME");
    			methods[cont] = name;
    			cont++;	
    		}
    		
		} catch (SQLException err) {
			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
		} catch (Exception err) {
			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
		}
		
    	// General values        
        JLabel		tituloSentencias, tituloFiltrado, tituloModulo, tituloQueries;
        JTextField	campoFiltro;
        JButton botonVolver, botonGuardar;
		final JButton botonAgregarMod;
		final JButton botonAgregarSen;
		final JButton botonQuitarSen;
        
        Icon	icono;
        
        /***********************************************************************************************/
        ImageIcon iconBack		=	null;
        ImageIcon iconSave		=	null;
        
        ImageIcon iconTitulo	=	null;
        ImageIcon iconAdd		=	null;
        
		try {
			Image img = ImageIO.read(new File("res/icons/Home-icon.png"));
			iconBack = new ImageIcon(img);
			
			img = ImageIO.read(new File("res/icons/Save-icon.png"));
			iconSave = new ImageIcon(img);
			
			img = ImageIO.read(new File("res/QCS_titulo.png"));
			iconTitulo = new ImageIcon(img);
			
			img = ImageIO.read(new File("res/icons/Add-icon.png"));
			iconAdd = new ImageIcon(img);
			
		} catch (IOException err) {
			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
		}
        
        tituloFiltrado = new JLabel("(Filtro)");
        tituloFiltrado.setBounds(20, 80, 140, 30); // x, y, ancho, alto
        miContenedor.add(tituloFiltrado);
        
        campoFiltro = new JTextField();
        tituloFiltrado.setLabelFor(campoFiltro);
        campoFiltro.setSize(470, 30);
        campoFiltro.setBounds(100, 90, 470, 30); // x, y, ancho, alto
        campoFiltro.setText("");
        campoFiltro.setEditable(true);
        campoFiltro.setVisible(true);
        
        campoFiltro.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				// Al introducir algo se filtran los metodos por un like desde la izquierda
				String valor = e.getActionCommand();
				valor = valor.toUpperCase();
				
				System.out.println("FILTRO: *" + valor + "*");
				
				if(valor == null || valor.equals("") || valor.length() == 0 ){
					
					try {
			    		
			    		// CARGANDO TODOS LOS METODOS
						String query = Sentence.GET_ALL_METHODS;
			    		rs = Query.executeSelect(query, prop);
			    		
			    		rs.last();
			    		methods = new String[rs.getRow()];
			    		rs.beforeFirst();
			    		
			    		int cont = 0;
			    		while (rs.next()){
			    			String name =	rs.getString("METHOD_NAME");
			    			methods[cont] = name;
			    			cont++;	
			    		}
			    		
					} catch (SQLException err) {
						LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
					} catch (Exception err) {
						LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
					}
					
				}else{
					
					try {
			    		
			    		// FILTRANDO METODOS
						String query = Sentence.GET_ALL_METHODS_FILTERED.replaceFirst("\\?",valor);
			    		rs = Query.executeSelect(query, prop);
			    		
			    		rs.last();
			    		methods = new String[rs.getRow()];
			    		rs.beforeFirst();
			    		
			    		int cont = 0;
			    		while (rs.next()){
			    			String name =	rs.getString("METHOD_NAME");
			    			methods[cont] = name;
			    			cont++;	
			    		}
			    		
					} catch (SQLException err) {
						LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
					} catch (Exception err) {
						LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
					}
					
				}
				
				listMet.setListData(methods);
				
			}
		});
        
        miContenedor.add(campoFiltro);
        
        tituloSentencias = new JLabel("METODOS");
        tituloSentencias.setBounds(20, 96, 140, 30); // x, y, ancho, alto
        miContenedor.add(tituloSentencias);
        
        // LISTA CON SCROLL DE METODOS
        listMet = new JList(methods);
        listMet.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        listMet.addListSelectionListener( new ListSelectionListener() {
			
			public void valueChanged(ListSelectionEvent e) {
				
        		String valor = (String)listMet.getSelectedValue();
        		
        		if(valor != null){
        			
        			// CARGAR SENTENCIAS
        	        try {
        	    		String query = Sentence.GET_ALL_SENTENCES_BY_METHOD_NAME.replaceFirst("\\?", valor);
        	    		rs = Query.executeSelect(query, prop);
        	    		
        	    		rs.last();
        	    		sentences = new String[rs.getRow()];
        	    		rs.beforeFirst();
        	    		
        	    		int cont = 0;
        	    		while (rs.next()){
        	    			String name =	rs.getString("SENTENCE_NAME");
        	    			sentences[cont] = name;
        	    			cont++;	
        	    		}
        	    		
        	    		listMod_Sen.setListData(sentences);
        	    		
        			} catch (SQLException err) {
        				LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
        			} catch (Exception err) {
        				LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
        			}
        			
        			// CARGAR SU MODULO
        			
        			try {
		        		
		    			String query = Sentence.GET_ALL_MODULES_BY_METHOD_NAME.replaceFirst("\\?",valor);
		        		rs = Query.executeSelect(query, prop);
		        		rs.first();
		        		
		        		String modulo = rs.getString("MODULE_NAME");
		        		
		        		if(modulo != null){
		        			campoModulo.setText(modulo);
		        		}else{
		        			campoModulo.setText("");
		        		}
		        		
		    		} catch (SQLException err) {
		    			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
		    		} catch (Exception err) {
		    			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
		    		}
        			
        		}
				
			}
			
		});
        
        scroll = new JScrollPane();
        scroll.getViewport().add(listMet);
        scroll.setPreferredSize(new Dimension(470,140));
        
        panelScroll = new JPanel();
        panelScroll.add(scroll);
        panelScroll.setBounds(100, 130, 470, 140);
        //frame.add(panelScroll);
        miContenedor.add(panelScroll);
        
        // MODULO
        tituloModulo = new JLabel("MODULO");
        tituloModulo.setBounds(20, 275, 80, 50); // x, y, ancho, alto
        miContenedor.add(tituloModulo);
        
        campoModulo = new JTextField();
        campoModulo.setSize(470, 30);
        campoModulo.setBounds(100, 280, 430, 30); // x, y, ancho, alto
        campoModulo.setText("");
        campoModulo.setEditable(false);
        miContenedor.add(campoModulo);
        
        tituloQueries= new JLabel("SENTENCIAS");
        tituloQueries.setBounds(20, 320, 80, 50); // x, y, ancho, alto
        miContenedor.add(tituloQueries);
        
        // BOTON
        icono = new ImageIcon("res/icons/Add-icon.png");
        
        botonAgregarMod = new JButton();
        botonAgregarMod.setBounds(540, 280, 30, 30); // x, y, ancho, alto
        botonAgregarMod.setIcon(iconAdd);
        botonAgregarMod.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		String valor = (String)listMet.getSelectedValue();
        		if(valor == null){
        			String msg = "Por favor, selecciona un modulo de la lista superior";
                	JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
        		}else{
        			
            		AgregarDialog customDialog = new AgregarDialog(AgregarDialog.MODULO, valor, prop, campoModulo, sentences, listMod_Sen);
            		
            		customDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
            		customDialog.setLocationRelativeTo(frame);
            		customDialog.setResizable(false);
            		customDialog.setVisible(true);
        			
        		}
        		
        	}
        });
        miContenedor.add(botonAgregarMod);
        
        botonAgregarSen = new JButton();
        botonAgregarSen.setIcon(icono);
        //botonAgregarSen.setBounds(40, 380, 30, 30); // x, y, ancho, alto
        botonAgregarSen.setBounds(100, 600, 30, 30); // x, y, ancho, alto
        botonAgregarSen.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		String valor = (String)listMet.getSelectedValue();
        		if(valor == null){
        			String msg = "Por favor, selecciona un modulo de la lista superior";
                    JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
        		}else{
        			
        			// Resetar el valor del array de sentencias que hay en la lista
        			sentences = new String[listMod_Sen.getModel().getSize()];
        			for(int x=0;x<listMod_Sen.getModel().getSize();x++){
        				sentences[x] = (String)listMod_Sen.getModel().getElementAt(x);
        			}
        			
        			AgregarDialog customDialog = new AgregarDialog(AgregarDialog.SENTENCE, valor, prop, campoModulo, sentences, listMod_Sen);
        			
            		customDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
            		customDialog.setLocationRelativeTo(frame);
            		customDialog.setResizable(false);
            		customDialog.setVisible(true);
            		
        		}
        		
        	}
        });
        miContenedor.add(botonAgregarSen);
        
        icono = new ImageIcon("res/icons/Delete-icon.png");
   
        botonQuitarSen = new JButton();
        botonQuitarSen.setIcon(icono);
        botonQuitarSen.setBounds(150, 600, 30, 30); // x, y, ancho, alto
        botonQuitarSen.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		int valor = listMod_Sen.getSelectedIndex();
        		
        		if(valor < 0){
        			String msg = "Por favor, selecciona una sentencia de la lista inferior";
                    JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
        		}else{
        			
        			// Resetar el valor del array de sentencias que hay en la lista
        			sentences = new String[listMod_Sen.getModel().getSize()];
        			for(int x=0;x<listMod_Sen.getModel().getSize();x++){
        				sentences[x] = (String)listMod_Sen.getModel().getElementAt(x);
        			}
        			
        			String[] listaValores				= sentences;
        			
        			Object[] objValues = listMod_Sen.getSelectedValues();
        			// Conversion de Object[] a String[]. Esto no se puede hacer con un cast, genera error en tiempo de ejecucion
        			String[] strValues = Arrays.copyOf(objValues, objValues.length, String[].class);
        			
        			sentences = new String[listaValores.length - strValues.length];
        			int counter = 0;
        			boolean flag = false;
        			
        			// Recorro todos los valores de la lista...
        			for(int x=0; x<listaValores.length;x++){
        				
        				flag = false;
        				
        				// Cada valor se comprueba en los seleccionados... 
        				for(int y=0; y<strValues.length;y++){
        					
        					if (listaValores[x] == strValues[y]){
        						// Si es de los seleccionados, entonces no cogere ese valor
        						flag = true;
        						break;
            				}
        					
        				}
        				if(!flag){
        					sentences[counter] = listaValores[x];
        					counter++;
        				}
        				
        			}
        			
        			listMod_Sen.setListData(sentences);
        			
        		}
        		
        	}
        });
        miContenedor.add(botonQuitarSen);
        
        // LISTA CON SCROLL DE SENTENCIAS Y/O MODULOS
        listMod_Sen = new JList();
        listMod_Sen.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        
        scroll = new JScrollPane();
        scroll.getViewport().add(listMod_Sen);
        scroll.setPreferredSize(new Dimension(470, 250));
        
        panelScroll = new JPanel();
        panelScroll.add(scroll);
        panelScroll.setBounds(100, 330, 470, 250);
        
        panelScroll.setVisible(true);
        
        miContenedor.add(panelScroll);
        
        // BOTON DE COMANDOS
        botonGuardar = new JButton("SAVE");
        botonGuardar.setIcon(iconSave);
        botonGuardar.setVerticalTextPosition(JButton.BOTTOM);
        botonGuardar.setHorizontalTextPosition(JButton.CENTER);
        botonGuardar.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		LogForDay.writeToLog("Estableciendo conexion a BBDD ", QCS.logPath, LogForDay.DEBUG);
        		Connection con = null;
				try {
					con = MainForm.getConexion(prop);
				} catch (SQLException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
				} catch (ClassNotFoundException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
				} catch (Exception err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
				}
        		
        		String msg;
        		int rowsInserted = 0;
        		
        		// Debe haber un modulo
        		if( campoModulo.getText() == null || campoModulo.getText().length() == 0  ){
        			msg = "Por favor, seleccione un modulo";
                    JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
        			return;
        		}else{
        			// Y al menos una query
        			if( listMod_Sen == null || listMod_Sen.getModel().getSize() < 1  ){
        				msg = "Por favor, agregue a la lista al menos una sentencia";
	                    JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
        				return;
        			}else{
        				
        				if( listMet == null || listMet.getModel().getSize() < 1 || listMet.getSelectedValue() == null  ){
        					msg = "Por favor, seleccione al menos un metodo de la lista";
    	                	JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
        					return;
            			}else{
            				
	                		Object[] objNuevosValores = new Object[listMod_Sen.getModel().getSize()];
	                		String[] listaDeSentencias = Arrays.copyOf(objNuevosValores, objNuevosValores.length, String[].class);
	                		String metodoSeleccionado = (String)listMet.getSelectedValue();
	                		
	                		// Nombre del modulo 		-> campoModulo
	                		// Nombre del metodo		-> metodoSeleccionado
	                		// Lista de las sentencias 	-> listaDeSentencias
	                		
	                		try {
	    		        		// Se actualiza el metodo
	    		    			String query = Sentence.UPDATE_MODULE_ID_IN_GCQS_METHOD.replaceFirst("\\?",campoModulo.getText());
	    		    			query = query.replaceFirst("\\?",metodoSeleccionado);
	    		        		int rowsUpdated = Query.executeUpdate(query, con);
	    		        		
	    		        		if(rowsUpdated != 1){
	    		        			con.rollback();
	    		        			
	    		        			msg = "No se actualizado el metodo correctamente";
	    		                    JOptionPane.showConfirmDialog(frame,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
	    		                    return;
	    		        		}
	    		        		
	    		        		// Se borran las relaciones de metodos porque se insertaran de nuevo
	    		        		query = Sentence.DELETE_ALL_RELATIONSHIPS_IN_GCQS_METHOD_QUERY.replaceFirst("\\?",metodoSeleccionado);
	    		        		Query.executeUpdate(query, con);
	    		        		
//	    		        		if(rowsDeleted == 0){
//	    		        			con.rollback();
//	    		        			
//	    		        			msg = "No se actualizado el metodo correctamente";
//	    		        			popup = new JFrame("Error");
//	    		                	popup.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//	    		                	popup.setSize(100, 100);
//	    		                    JOptionPane.showMessageDialog(popup, msg, null, JOptionPane.ERROR_MESSAGE);
//	    		                    return;
//	    		        		}
	    		        		
	    		        		for(int count = 0; count<listaDeSentencias.length; count++){
	                    			
	                    			// Se actualiza cada query
	    		        			query = Sentence.UPDATE_MODULE_ID_IN_GCQS_QUERY.replaceFirst("\\?",campoModulo.getText());
	    		        			query = query.replaceFirst("\\?", (String)listMod_Sen.getModel().getElementAt(count));
		    		        		rowsUpdated = Query.executeUpdate(query, con);
		    		        		
		    		        		if(rowsUpdated != 1){
		    		        			con.rollback();
		    		        			
		    		        			msg = "No se actualizado el metodo correctamente";
		    		                    JOptionPane.showConfirmDialog(frame,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
		    		                    return;
		    		        		}
		    		        		
	                    			// Se actualiza cada relacion metodo <> query
		    		        		query = Sentence.INSERT_RELATIONSHIP_IN_GCQS_METHOD_QUERY.replaceFirst("\\?", metodoSeleccionado);
	    		        			query = query.replaceFirst("\\?", (String)listMod_Sen.getModel().getElementAt(count));
	    		        			query = query.replaceFirst("\\?", String.valueOf(count+1));
		    		        		rowsInserted = Query.executeUpdate(query, con);
		    		        		
		    		        		if(rowsInserted != 1){
		    		        			con.rollback();
		    		        			
		    		        			msg = "No se actualizado el metodo correctamente";
		    		                    JOptionPane.showConfirmDialog(frame,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
		    		                    return;
		    		        		}
		    		        		
	                    		}
	    		        		
	    		        		con.commit();
	    		        		
	    		        		msg = "El metodo se actualiz� correctamente";
	    	                	JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.INFORMATION_MESSAGE);
	    	                    
	    	                    frame.setContentPane(mainContenedor);
	    	                    frame.setVisible(true);
	    	                    
	    	                    // Refrescar los modulos
								// LISTA DE LOS MODULOS
						        ResultSet rs = Query.executeSelect(Sentence.GET_ALL_MODULES, prop);
						        String[] modules = null;
						               
						        try {
									rs.last();
									modules = new String[rs.getRow()];
									rs.beforeFirst();
								} catch (SQLException err) {
									LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
								}
						        
						    	try {
						    		int cont = 0;
									while (rs.next()){
										String name =	rs.getString("MODULE_NAME");
										modules[cont] = name;
										cont++;
									}
									rs.close();
								} catch (SQLException err) {
									LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
								}
			        			
								listModPrev.setListData(modules);
	    	                    
	    	                    // Ejecutar la query y poner los valores a listMet
	    	                	String nameMethod = (String) listMetPrev.getSelectedValue();
	    	                	String[][] params = Query.sentenceAddParam(Query.TYPE_STRING, nameMethod, 1);
	    	                	
	    	                	String[] queries = null;
	    	                	
	    	                    try {
	    	                    	
	    	                    	rs = Query.executeQueryWithParams(Sentence.GET_TOTAL_QUERIES_BY_METHOD_NAME, params, prop);
	    	                    	rs.next();
	    	                    	queries	= new String[rs.getInt("TOTAL")];
	    	                    	
	    	                    	rs = Query.executeQueryWithParams(Sentence.GET_QUERY_BY_METHOD_NAME, params, prop);
	    	                		int cont = 0;
	    	                		
	    	            			while (rs.next()){
	    	            				String name =	rs.getString("QUERY_NAME");
	    	            				queries[cont] = name;
	    	            				cont++;
	    	            			}
	    	            			
	    	            			rs.close();
	    	            			Query.stmt.close();
	    	            			
	    	            		} catch (SQLException err) {
	    	            			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
	    	            			queries	= new String[0];
	    	            		} catch (Exception err) {
	    	            			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
	    						}
	    	            		
	    	            		// REFRESH VALUES
	    	            		listSenPrev.setListData(queries);
	    	                    
	    		    		} catch (SQLException err) {
	    		    			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
	    		    		} catch (Exception err) {
	    		    			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
	    		    		}
    		    		
    		    		
            			}
                		
        			}
        		}
        		
        		try {
					MainForm.closeConexion(con);
					LogForDay.writeToLog("Conexxion BBDD cerrada", QCS.logPath, LogForDay.DEBUG);
				} catch (SQLException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
				} catch (ClassNotFoundException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
				}
        		
        	}
        });
        miContenedor.add(botonGuardar);
        
        botonVolver = new JButton("BACK");
        botonVolver.setIcon(iconBack);
        botonVolver.setVerticalTextPosition(JButton.BOTTOM);
        botonVolver.setHorizontalTextPosition(JButton.CENTER);
        botonVolver.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		int value = JOptionPane.showConfirmDialog(frame,"�Deseas volver sin guardar las modificaciones?","VOLVER",JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
        		
        		if(value == JOptionPane.OK_OPTION ){
        			frame.setContentPane(mainContenedor);
                    frame.setVisible(true);
                    
                    // Ejecutar la query y poner los valores a listMet
                	String nameMethod = (String) listMetPrev.getSelectedValue();
                	String[][] params = Query.sentenceAddParam(Query.TYPE_STRING, nameMethod, 1);
                	
                	String[] queries = null;
                	
                    try {
                    	
                    	rs = Query.executeQueryWithParams(Sentence.GET_TOTAL_QUERIES_BY_METHOD_NAME, params, prop);
                    	rs.next();
                    	queries	= new String[rs.getInt("TOTAL")];
                    	
                    	rs = Query.executeQueryWithParams(Sentence.GET_QUERY_BY_METHOD_NAME, params, prop);
                		int cont = 0;
                		
            			while (rs.next()){
            				String name =	rs.getString("QUERY_NAME");
            				queries[cont] = name;
            				cont++;
            			}
            			
            			rs.close();
            			Query.stmt.close();
            			
            		} catch (SQLException err) {
            			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
            			queries	= new String[0];
            		} catch (Exception err) {
            			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
					}
            		
            		// REFRESH VALUES
            		listSenPrev.setListData(queries);
                    
        		}
        	}
        });
        miContenedor.add(botonVolver);

        JLabel label = new JLabel();
        label.setBounds(20, 640, 620, 140); // x, y, ancho, alto
        label.setIcon(iconTitulo);
        
        miContenedor.add(label);
        
        // TOOLBAR
        JToolBar toolbar = new JToolBar();
        toolbar.setSize(700, 75);
        toolbar.setFloatable(false);
        
        toolbar.add(botonGuardar);
        toolbar.addSeparator(); //SEPARADOR
        toolbar.add(botonVolver);
        
        miContenedor.add(toolbar);
        
	}

}
