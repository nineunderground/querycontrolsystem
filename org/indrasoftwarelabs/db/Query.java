package org.indrasoftwarelabs.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.indrasoftwarelabs.log.LogForDay;
import org.indrasoftwarelabs.main.MainForm;
import org.indrasoftwarelabs.main.QCS;

public class Query {
	
	public static final int TYPE_STRING		= 1;
	public static final int TYPE_INT		= 2;
	public static final int TYPE_DATE		= 3;
	public static final int TYPE_DATETIME	= 4;
	
	public static final String NAME_STRING		= "1";
	public static final String NAME_INT			= "2";
	public static final String NAME_DATE		= "3";
	public static final String NAME_DATETIME	= "4";

	private static Connection	dbConn;
	
	public static PreparedStatement stmt;
	
	static Thread hilo = null;
	
	public Query() {
		// TODO Auto-generated constructor stub
	}
	
	public static Connection getConexionThread(Properties prop) throws SQLException, ClassNotFoundException, Exception {
		
		LogForDay.writeToLog("Obteniendo conexion de BBDD" , QCS.logPath, LogForDay.DEBUG);
		
		//Connection	dbConn;
		
		String usr		= prop.getProperty("DB_USER");
		String pwd		= prop.getProperty("DB_PASS");
		String sid		= prop.getProperty("DB_SID");
		String host		= prop.getProperty("DB_HOST");
		String port		= prop.getProperty("DB_PORT");
		String urlBase	= prop.getProperty("DB_URL_BASE");
		String DBDriver	= prop.getProperty("DB_DRIVER");
        
        String url = urlBase + ":" + usr + "/" + pwd + "@" + host + ":" + port + ":" + sid;
        
        Class.forName(DBDriver);
        
        new DBThread().start();
        	
        	dbConn = DriverManager.getConnection(url);
            dbConn.setAutoCommit(false);
        
        LogForDay.writeToLog("Conexion de BBDD creada" , QCS.logPath, LogForDay.DEBUG);
        
		return dbConn;
	}
	
	public static class DBThread extends Thread {
		
		  private int countDown = 5;
		  
		  public DBThread() {
		    start();
		  }
		  
//		  public String toString() {
//		    return "#" + getName() + ": " + countDown;
//		  }
		  
		  public void run() {
		    while(true) {
		    	
		      System.out.println(this);
		      if( --countDown == 0) return;
		      
		      try {
		    	  Thread.sleep(1000);
		      } catch (InterruptedException err) {
		    	  System.out.println("Hilo interrumpido: " + err.getLocalizedMessage());  
		      }
		      
		    }
		  }
		  
	}

	public static ResultSet executeQuery(String query, Properties prop) throws ClassNotFoundException, Exception{
		
		LogForDay.writeToLog("Estableciendo conexion a BBDD ", QCS.logPath, LogForDay.DEBUG);
		Connection con = MainForm.getConexion(prop);
		
		ResultSet rs;
		
		Statement stmt = con.createStatement();
		rs = stmt.executeQuery(query);
		
		LogForDay.writeToLog("Query ejecutada " + query, QCS.logPath, LogForDay.INFO);
		
		MainForm.closeConexion(con);
		LogForDay.writeToLog("Conexion BBDD cerrada", QCS.logPath, LogForDay.DEBUG);
		
		return rs;
	}
	
	public static ResultSet executeQueryByConn(String query, Connection con) throws ClassNotFoundException, Exception{
		
		ResultSet rs;
		
		Statement stmt = con.createStatement();
		rs = stmt.executeQuery(query);
		
		LogForDay.writeToLog("Query ejecutada:\n" + query, QCS.logPath, LogForDay.INFO);
		
		return rs;
	}
	
	public static ResultSet executeSelect(String query, Properties prop) throws SQLException, ClassNotFoundException, Exception {
		
		ResultSet rs;
		
		LogForDay.writeToLog("Estableciendo conexion a BBDD ", QCS.logPath, LogForDay.DEBUG);
		Connection con = MainForm.getConexion(prop);
		
		try {
			//PreparedStatement stmt = con.prepareStatement(query);
			Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			rs = stmt.executeQuery(query);
			
		} catch (SQLException e) {
			LogForDay.writeToLog("Query error " + e.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			return null;
		} catch (Exception e) {
			LogForDay.writeToLog("Query error " + e.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			return null;
		}
		
		LogForDay.writeToLog("Query ejecutada " + query, QCS.logPath, LogForDay.INFO);
        
		MainForm.closeConexion(con);
		LogForDay.writeToLog("Conexxion BBDD cerrada", QCS.logPath, LogForDay.DEBUG);
		
		return rs;
	}
	
	public static ResultSet executeSelectByConn(String query, Connection con) throws SQLException, ClassNotFoundException, Exception {
		
		ResultSet rs;
		
		try {
			//PreparedStatement stmt = con.prepareStatement(query);
			Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			rs = stmt.executeQuery(query);
			
		} catch (SQLException e) {
			LogForDay.writeToLog("Query error " + e.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			return null;
		} catch (Exception e) {
			LogForDay.writeToLog("Query error " + e.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			return null;
		}
		
		LogForDay.writeToLog("Query ejecutada " + query, QCS.logPath, LogForDay.INFO);
        
		return rs;
	}
	
	public static int executeUpdate(String query, Connection con) throws SQLException, Exception {
		
		int rows = 0;
		
		Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
		
		rows = stmt.executeUpdate(query);
		stmt.close();
		
		LogForDay.writeToLog("Query ejecutada " + query, QCS.logPath, LogForDay.INFO);
		
		return rows;
	}
	
	public static int executeInsert(String query, Connection con) throws SQLException, ClassNotFoundException, Exception {
		
		LogForDay.writeToLog("Estableciendo conexion a BBDD ", QCS.logPath, LogForDay.DEBUG);
		
		int registersInserted;
		
		try {
			Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			registersInserted = stmt.executeUpdate(query);
			stmt.close();
		} catch (SQLException e) {
			//e.printStackTrace();
			LogForDay.writeToLog("Query error " + e.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			return 0;
		} catch (Exception e) {
			LogForDay.writeToLog("Query error " + e.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			return 0;
		}
		
		LogForDay.writeToLog("Query ejecutada " + query, QCS.logPath, LogForDay.INFO);
        
		return registersInserted;
	}
	
	public static int executeDelete(String query, Connection con) throws SQLException, ClassNotFoundException, Exception {
		
		int registersInserted;
		
		try {
			Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			registersInserted = stmt.executeUpdate(query);
			stmt.close();
		} catch (SQLException e) {
			System.out.println("ERROR EN LA QUERY: " + query);
			LogForDay.writeToLog("Query error " + e.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			return 0;
		} catch (Exception e) {
			LogForDay.writeToLog("Query error " + e.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			return 0;
		}
		
		LogForDay.writeToLog("Query ejecutada " + query, QCS.logPath, LogForDay.INFO);
        
		return registersInserted;
	}
	
	public static ResultSet executeQueryWithParams(String query, String[][] params, Properties prop) throws Exception {
		
		LogForDay.writeToLog("Estableciendo conexion a BBDD ", QCS.logPath, LogForDay.DEBUG);
		Connection con = MainForm.getConexion(prop);
		
		ResultSet rs;
		
		try {
			
			int param = 0;
			stmt = con.prepareStatement(query);
			
			for(int x=0; x<params.length; x++){
				
				param = Integer.parseInt(params[x][0]);
				
				switch( param ){
				
					case TYPE_STRING:
						stmt.setString(x+1, params[0][1]);
						break;
					case TYPE_INT:
						stmt.setInt(x+1, Integer.parseInt(params[0][1]));
						break;
					case TYPE_DATE:
						//stmt.setInt(x++, Integer.parseInt(params[0][0]));
						break;
					case TYPE_DATETIME:
						//stmt.setInt(x++, Integer.parseInt(params[0][0]));
						break;
					case 0:
						throw new Exception("Error de parametros en la sentencia: " + query);
					
				}
				
			}
			
			rs = stmt.executeQuery();
			
		} catch (SQLException e) {
			LogForDay.writeToLog("Query error " + e.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			return null;
		} catch (Exception e) {
			LogForDay.writeToLog("Query error " + e.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			return null;
		}
		
		LogForDay.writeToLog("Query ejecutada " + query, QCS.logPath, LogForDay.INFO);
        
		MainForm.closeConexion(con);
		LogForDay.writeToLog("Conexxion BBDD cerrada", QCS.logPath, LogForDay.DEBUG);
		
		return rs;
	}
	
	public static ResultSet executeQueryWithParamsByConn(String query, String[][] params, Connection con) throws Exception {
		
		LogForDay.writeToLog("Estableciendo conexion a BBDD ", QCS.logPath, LogForDay.DEBUG);
		
		ResultSet rs;
		
		try {
			
			int param = 0;
			stmt = con.prepareStatement(query);
			
			for(int x=0; x<params.length; x++){
				
				param = Integer.parseInt(params[x][0]);
				
				switch( param ){
				
					case TYPE_STRING:
						stmt.setString(x+1, params[0][1]);
						break;
					case TYPE_INT:
						stmt.setInt(x+1, Integer.parseInt(params[0][1]));
						break;
					case TYPE_DATE:
						//stmt.setInt(x++, Integer.parseInt(params[0][0]));
						break;
					case TYPE_DATETIME:
						//stmt.setInt(x++, Integer.parseInt(params[0][0]));
						break;
					case 0:
						throw new Exception("Error de parametros en la sentencia: " + query);
					
				}
				
			}
			
			rs = stmt.executeQuery();
			
		} catch (SQLException e) {
			LogForDay.writeToLog("Query error " + e.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			return null;
		} catch (Exception e) {
			LogForDay.writeToLog("Query error " + e.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			return null;
		}
		
		LogForDay.writeToLog("Query ejecutada " + query, QCS.logPath, LogForDay.INFO);
        
		return rs;
	}
	
	public static String[][] sentenceAddParam(int type, String value, int totalParams) {
		
		String[][] params = new String[totalParams][2];
		
        params[0][0] = Query.NAME_STRING;
        params[0][1] = value;
        
        return params;
		
	}
	
}
