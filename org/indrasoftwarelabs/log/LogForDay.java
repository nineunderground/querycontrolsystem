package org.indrasoftwarelabs.log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Se establece un fichero de log por dia
 * @author irjimenez
 *
 */

public class LogForDay {
	
	/**
	 * Dependiendo del modo de escritura en el log. Se filtraran unas trazas u otras segun el valor 
	 * del parametro accessMode.
	 * 
	 * El valor de accessMode indica el tope maximo del nivel de trazas que tendra el log
	 * 
	 * Los niveles son los siguientes:
	 * 
	 * 0.- ERROR. Trazas de errores capturados en excepciones u otros errores.
	 * 1.- DEBUG. Trazas de debugs para establecer el flujo de codigo en caso de depuracion de errores .
	 * 2.- WARNING. Se ve hasta las trazas usadas para mostrar posibles alertas que no llegan a tener categoria de error
	 * 3.- INFO. Se muestran hasta trazas a modo de información
	 * 4.- ALL . Se escribe siempre la traza independientemente del parametro.
	 * 
	 */
	
	public static int accessMode = 0;
	
	public static final int ERROR 	= 0;
	public static final int WARNING = 1;
	public static final int DEBUG	= 2;
	public static final int INFO 	= 3;
	public static final int ALWAYS	= 4; 
	
	public static int[] 	modos	= new int[]{ERROR,WARNING,DEBUG,INFO,ALWAYS};
	
	public static String[] 	modosTexto	= new String[]{"ERROR","WARNING","DEBUG","INFO","ALL TRACES"};
	
	public static void writeToLog(String lineaLog, String strPathLogs, int nivelTraza){
	    
        //String strPathLogs = propGenerales.getProperty("QCS_RUTA_LOGS");
        //String strPathLogsComprimidos = propGenerales.getProperty("RUTA_LOGS_COMPRIMIDOS");
	    
	    File f = new File(strPathLogs.concat( getLogFileName() ));
	    
	    try{
	        
	        String text;
	        
	        if(!f.exists()){
	            
	        	// Se borra el log del dia anterior
	        	final String filterName = "QCS.log";
	        	File dir = new File(strPathLogs);
	        	
	        	FilenameFilter filter = new FilenameFilter() {
	        	    public boolean accept(File dir, String name) {
	        	        return name.endsWith(filterName);
	        	    }
	        	};
	        	String children[] = dir.list(filter);
	        	
	        	for(int x=0;x<children.length;x++){
	        		
	        		File ficheroBorrable = new File(children[x]);
	        		
	        		if(ficheroBorrable.exists()){
	        			ficheroBorrable.delete();
	        		}
	        		
	        	}
	        	
	            // Se crea un fichero nuevo para el dia actual
	            PrintStream logFile = new PrintStream(new FileOutputStream(f, true));
	            
	            // Cabecera para el archivo nuevo
	            text = "Fichero de log creado. OK";
	            logFile.println(text);
	            String ip = java.net.InetAddress.getLocalHost().getHostAddress();
	            text = "El Log escribe las trazas hasta el nivel de tipo -> " + LogForDay.modosTexto[LogForDay.accessMode] + " desde la IP: " + ip;
	            logFile.println(text);
	            text = "--------------------------------------------------------------";
	            logFile.println(text);
	            
	        }
	        
	        // Si la traza esta bajo el nivel del modo de log, entonces si se escribe
            if( modos[nivelTraza] <= LogForDay.accessMode  ){
            	
            	PrintStream logFile = new PrintStream(new FileOutputStream(f, true));
    	        String	fechaHoraLog	=	"";
    	        
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd ' ' hh:mm:ss a");
                fechaHoraLog =  sdf.format(cal.getTime());
                
                lineaLog = fechaHoraLog + " : " + modosTexto[nivelTraza] + " - " + lineaLog;
                
            	// Se escribe la entrada de session en el log
    	        logFile.println(lineaLog);
    	        logFile.close();
    	        
            }
            
	    }catch(IOException err){
	        
	        //trazas.debug ("No hay fichero :( ");
	        
	    }
	    
	}
	
	
	/**
	 * Establece el nombre del fichero de log sobre el que tiene que escribir
	 * @return
	 */
	private static	String getLogFileName(){
	    
	    String strFileName	=	"";
	    
	    Calendar 			cal	=	Calendar.getInstance();
        SimpleDateFormat	sdf	=	new SimpleDateFormat("yyyy_MM_dd");
        strFileName 			= 	sdf.format(cal.getTime());
	    
        strFileName 			=	strFileName.concat("_QCS.log");
        
        return strFileName;
        
	}
	

}
