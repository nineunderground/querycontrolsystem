package org.indrasoftwarelabs.dialogs;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

public class ResultDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	ResultDialog frame;
	int limitRows = 10;
	
	public ResultDialog(final JFrame ed, ResultSet res, int width, int height) throws SQLException, Exception{
		
		frame = this;
		
		// General values
    	setModalityType(ModalityType.APPLICATION_MODAL);
    	setTitle("RESULTADO");
    	setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(width, height);
        setResizable(false);
        
        final JPanel panel = new JPanel();
        panel.setLayout(null);
        
        DefaultTableModel model = new DefaultTableModel();
        
        int totalColumns = res.getMetaData().getColumnCount();
        
        Object[][] rowData = new Object[limitRows][totalColumns];
        
        for(int x=0; x<totalColumns;x++){
        	model.addColumn( res.getMetaData().getColumnName(x+1) );
        }
//        String columnaVacia = "";
//        model.addColumn(columnaVacia);
        
        int x=0;
        
        // Se recorre cada fila
        while(x<limitRows && res.next()){
        	
        	// Se rellenan todas las columnas
        	for(int y=0; y<totalColumns;y++){
        		
        		switch (res.getMetaData().getColumnType(y+1))
        		{
        			case Types.VARCHAR:
        				rowData[x][y] = res.getString(y+1);
        				break;
        				
        			case Types.INTEGER:
        				rowData[x][y] = res.getInt(y+1);
        				break;
        			case Types.BOOLEAN:
        				rowData[x][y] = res.getBoolean(y+1);
        				break;
        				
        			case Types.FLOAT:
        				rowData[x][y] = res.getFloat(y+1);
        				break;
        				
        			case Types.DATE:
        				rowData[x][y] = res.getDate(y+1);
        				break;
        				
        			case Types.DOUBLE:
        				rowData[x][y] = res.getDouble(y+1);
        				break;
        				
        			case Types.BINARY:
        				rowData[x][y] = res.getBinaryStream(y+1);
        				break;
        				
        			case Types.TIMESTAMP:
        				rowData[x][y] = res.getTimestamp(y+1);
        				break;
        				
        			case Types.TIME:
        				rowData[x][y] = res.getTime(y+1);
        				break;
        				
        			case Types.CLOB:
        				rowData[x][y] = res.getClob(y+1);
        				break;
        				
        			case Types.NUMERIC:
        				rowData[x][y] = res.getInt(y+1);
        				break;
        		}
        		
        	}
        	
        	model.addRow(rowData[x]);
        	
        	x++;
        }
        
        // JTable
        JTable table = new JTable(model) {
        	private static final long serialVersionUID = 1L;
			// Es una tabla no editable
			public boolean isCellEditable(int row,int column){
				return false;
			}
        };
        
        table.setBounds(20, 20, 400, 300); // x, y, ancho, alto
        table.setRowHeight(30);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        CustomRender cr = new CustomRender();
        TableColumn col;
        
        // Se recorren las columnas para ponerles un ancho fijo
        for(int z=0; z<totalColumns; z++){
        	col = table.getColumnModel().getColumn(z);
        	col.setPreferredWidth(90);
        	
        	table.getColumnModel().getColumn(z).setCellRenderer(cr);
        }
        
        // La ultima columna es falsa
//        col = table.getColumnModel().getColumn(totalColumns);
//    	col.setPreferredWidth(1);
    	
        JTableHeader header = table.getTableHeader();
        header.setBackground(Color.GRAY);
        header.setPreferredSize(new Dimension(header.getPreferredSize().width,30));
        
        JScrollPane sp = new JScrollPane(table);
	    sp.setBounds(20, 20, 400, 300); // x, y, ancho, alto
	    sp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
	    sp.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	    
	    getContentPane().add(sp);
        
        // OK
        JButton botonVolver = new JButton();
        botonVolver.setText("OK");
        botonVolver.setBounds(180, 330, 80, 30); // x, y, ancho, alto
        botonVolver.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		frame.dispose();
        	}
        });
        panel.add(botonVolver);
        
        add(panel);
        
	}
	
	/**
	 * Es una tabla propia para poder manipular cuando renderizar el color del fondo de las celdas a mi gusto
	 * @author irjimenez
	 *
	 */
	public class CustomRender extends JLabel implements TableCellRenderer
	{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		TableColumn col;
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			
			table.setBackground(Color.BLACK);
			
			if( row==0 || row%2==0 )
			{
	            setForeground(Color.WHITE);
			}else{
				setForeground(Color.GREEN);
			}
			
//			System.out.print(" ROW: " + row );
//			System.out.print(" COLUMN: " + column);
//			System.out.println(" VALOR: " + value);
			
			if(value != null){
				if( value.getClass().equals(String.class) ){
					setText(value.toString());
				}else if( value.getClass().equals(Integer.class) ){
					setText( String.valueOf(value) );
				}else if( value.getClass().equals(java.sql.Timestamp.class) ){
					setText( String.valueOf(value) );
				}else if( value.getClass().equals(Date.class) ){
					setText( String.valueOf(value) );
				}
			}
			
			return this;
			
		}
		
	} 
	
}
