package org.indrasoftwarelabs.dialogs;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import org.indrasoftwarelabs.db.Query;
import org.indrasoftwarelabs.db.Sentence;
import org.indrasoftwarelabs.log.LogForDay;
import org.indrasoftwarelabs.main.QCS;

public class AgregarDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	AgregarDialog frame;
	final	static int	NEW_PARAMETER	= 1;
	final	static int	EDIT_PARAMETER	= 2;

	public static final int MODULO	= 1;
	public static final int SENTENCE	= 2;
	
	private String[]	listaAux;
	private String[]	listaAuxSentences;
	
	public AgregarDialog(final int mode, final String name, final Properties prop, final JTextField	campoModulo, final String[] sentencesPrev, final JList listMod_Sen){
		
		frame = this;
		
		final ResultSet rs;
		
		String[] modules;
		String[] sentences;
		String[] listaValores = null;
		
		// General values
    	setModalityType(ModalityType.APPLICATION_MODAL);
    	
    	switch(mode){
    	
    	case MODULO:
    		
    		setTitle("SELECCIONA UN MODULO");
    		
    		try {
        		
        		// CARGANDO TODOS LOS METODOS
        		rs = Query.executeSelect(Sentence.GET_ALL_MODULES, prop);
        		
        		rs.last();
        		modules = new String[rs.getRow()];
        		rs.beforeFirst();
        		
        		int cont = 0;
        		while (rs.next()){
        			modules[cont] = rs.getString("MODULE_NAME");
        			cont++;	
        		}
        		
        		listaValores = modules;
        		
    		} catch (SQLException err) {
    			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
    		} catch (Exception err) {
    			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
    		}
    		
    		break;
    		
    	case SENTENCE:
    		
    		setTitle("SELECCIONA LAS SENTENCIAS");
    		
    		try {
        		
        		// CARGANDO TODOS LOS METODOS
        		rs = Query.executeSelect(Sentence.GET_ALL_QUERIES, prop);
        		
        		rs.last();
        		sentences = new String[rs.getRow()];
        		rs.beforeFirst();
        		
        		int cont = 0;
        		while (rs.next()){
        			sentences[cont] = rs.getString("QUERY_NAME");
        			cont++;	
        		}
        		
        		listaValores = sentences;
        		
    		} catch (SQLException err) {
    			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
    		} catch (Exception err) {
    			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
    		}
    		
    		break;
    	
    	}
    	
    	setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(300, 400);
        
        final JPanel panel = new JPanel();
        panel.setLayout(null);
        
        JLabel		etiqueta;
        JTextField	filtro;
        JButton		botonCancelar = new JButton();
        JButton		botonAceptar = new JButton();
        final JList		lista;
        JScrollPane	scroll;
        JPanel		panelScroll;
        
        etiqueta = new JLabel("Nombre: ");
        etiqueta.setBounds(20, 5, 100, 30); // x, y, ancho, alto
        panel.add(etiqueta);
        
        lista = new JList(listaValores);
        
        switch(mode){
    	
    	case MODULO:
    		lista.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    		break;
    	case SENTENCE:
    		lista.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    		break;
		}
        
        scroll = new JScrollPane();
        scroll.getViewport().add(lista);
        scroll.setPreferredSize(new Dimension(250,260));
        
        panelScroll = new JPanel();
        panelScroll.add(scroll);
        panelScroll.setBounds(20, 50, 250, 260);
        panel.add(panelScroll);
        
        botonCancelar.setText("CANCELAR");
        botonCancelar.setBounds(180, 330, 100, 30); // x, y, ancho, alto
        
        botonCancelar.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		frame.dispose();
        	}
        });
        panel.add(botonCancelar);
        
        botonAceptar.setText("ACEPTAR");
        botonAceptar.setBounds(40, 330, 100, 30); // x, y, ancho, alto
        
        botonAceptar.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		int count;
        		switch(mode){
            	
            	case MODULO:
            		
            		campoModulo.setText((String)lista.getSelectedValue());
            		
            		break;
            	case SENTENCE:
            		
            		listaAux			= sentencesPrev;
            		listaAuxSentences 	= sentencesPrev;
            		
            		int nuevos = lista.getSelectedValues().length;
            		int limite = 0;
            		
            		Object[] nuevosValores = lista.getSelectedValues();
            		
            		listaAuxSentences = new String[listaAux.length + nuevos];
            		
            		for(count=0; count<listaAux.length;count++){
            			listaAuxSentences[count] = listaAux[count];
            			limite = count+1;
            		}
            		
            		for(count=0; count<nuevos;count++){
            			
            			// Si ya existe salta una alerta...
            			for(int rec=0;rec<listaAux.length;rec++){
            				if( ((String)nuevosValores[count]).equals(listaAux[rec]) ){
            					
            					String msg = "La sentencia " + listaAux[rec] + " ya se encuentra a�adida. Por favor eliminila de la lista";
                        		
                        		JFrame popup = new JFrame();
                            	popup.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                            	popup.setSize(100, 100);
                            	
                                JOptionPane.showConfirmDialog(frame,msg,"About us",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
            					
            					return;
            				}
            			}
            			
            			listaAuxSentences[limite+count] = (String)nuevosValores[count];
            		}
            		
            		//sentencesPrev = listaAuxSentences;
            		//listMod_Sen.setListData(sentencesPrev);
            		listMod_Sen.setListData(listaAuxSentences);
            		
            		break;
        		}
        		
        		frame.dispose();
        	}
        });
        panel.add(botonAceptar);
        
        filtro = new JTextField();
        filtro.setBounds(100, 5, 180, 30); // x, y, ancho, alto
        
        filtro.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				ResultSet rs;
				String[] modulesAux		= null;
				String[] sentencesAux	= null;
				String query;
				
				// Al introducir algo se filtran los metodos por un like desde la izquierda
				String valor = e.getActionCommand();
				valor = valor.toUpperCase();
				
				System.out.println("FILTRO: *" + valor + "*");
				
				if(valor == null || valor.equals("") || valor.length() == 0 ){
					
					switch(mode){
			    	
			    	case MODULO:
			    		
			    		setTitle("SELECCIONA UN MODULO PARA EL METODO");
			    		
			    		try {
			        		
			        		// CARGANDO TODOS LOS METODOS
			        		rs = Query.executeSelect(Sentence.GET_ALL_MODULES, prop);
			        		
			        		rs.last();
			        		modulesAux = new String[rs.getRow()];
			        		rs.beforeFirst();
			        		
			        		int cont = 0;
			        		while (rs.next()){
			        			modulesAux[cont] = rs.getString("MODULE_NAME");
			        			cont++;	
			        		}
			        		
			    		} catch (SQLException err) {
			    			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			    		} catch (Exception err) {
			    			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			    		}
			    		
			    		lista.setListData(modulesAux);
			    		
			    		break;
			    		
			    	case SENTENCE:
			    		
			    		setTitle("SELECCIONA LAS SENTENCIAS PARA EL METODO");
			    		
			    		try {
			        		
			        		// CARGANDO TODOS LOS METODOS
			        		rs = Query.executeSelect(Sentence.GET_ALL_QUERIES, prop);
			        		
			        		rs.last();
			        		sentencesAux = new String[rs.getRow()];
			        		rs.beforeFirst();
			        		
			        		int cont = 0;
			        		while (rs.next()){
			        			sentencesAux[cont] = rs.getString("QUERY_NAME");
			        			cont++;	
			        		}
			        		
			    		} catch (SQLException err) {
			    			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			    		} catch (Exception err) {
			    			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			    		}
			    		
			    		lista.setListData(sentencesAux);
			    		
			    		break;
			    	
			    	}
					
				}else{
					
					switch(mode){
			    	
			    	case MODULO:
			    		
			    		setTitle("FILTRA LOS MODULOS");
			    		
			    		try {
			        		
			        		// FILTRANDO LOS MODULOS
			    			query = Sentence.GET_ALL_MODULES_FILTERED.replaceFirst("\\?",valor);
			        		rs = Query.executeSelect(query, prop);
			        		
			        		rs.last();
			        		modulesAux = new String[rs.getRow()];
			        		rs.beforeFirst();
			        		
			        		int cont = 0;
			        		while (rs.next()){
			        			modulesAux[cont] = rs.getString("MODULE_NAME");
			        			cont++;	
			        		}
			        		
			    		} catch (SQLException err) {
			    			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			    		} catch (Exception err) {
			    			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			    		}
			    		
			    		lista.setListData(modulesAux);
			    		
			    		break;
			    		
			    	case SENTENCE:
			    		
			    		setTitle("FILTRA LAS SENTENCIAS");
			    		
			    		try {
			    			
			    			// FILTRANDO LAS SENTENCIAS
			    			query = Sentence.GET_ALL_QUERIES_FILTERED.replaceFirst("\\?",valor);
			        		rs = Query.executeSelect(query, prop);
			        		
			        		rs.last();
			        		sentencesAux = new String[rs.getRow()];
			        		rs.beforeFirst();
			        		
			        		int cont = 0;
			        		while (rs.next()){
			        			sentencesAux[cont] = rs.getString("QUERY_NAME");
			        			cont++;	
			        		}
			        		
			    		} catch (SQLException err) {
			    			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			    		} catch (Exception err) {
			    			LogForDay.writeToLog(" " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			    		}
			    		
			    		lista.setListData(sentencesAux);
			    		
			    		break;
			    	
			    	}
					
				}
				
			}
		});
        
        panel.add(filtro);
        
        add(panel);
        
	}
	
}
