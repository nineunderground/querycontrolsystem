package org.indrasoftwarelabs.dialogs;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.indrasoftwarelabs.containers.EditContainer;

@SuppressWarnings("serial")
public class SeleccionarDialog extends JDialog {

	SeleccionarDialog frame;
	public final	static int	NEW_PARAMETER	= 1;
	public final	static int	EDIT_PARAMETER	= 2;
	
	final JRadioButton optionModule		= new JRadioButton("Modulo", false);
    final JRadioButton optionMethod		= new JRadioButton("Metodo", false);
    final JRadioButton optionSentence	= new JRadioButton("Sentencia", true);
	
	public SeleccionarDialog(	final int mode,
								final String objetoEditable,
								final JFrame ventana,
								final Properties prop,
								final Container mainContenedor){
		
		frame = this;
		
		// General values
    	setModalityType(ModalityType.APPLICATION_MODAL);
    	setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(260, 250);
        
        final JPanel panel = new JPanel();
        panel.setLayout(null);
        
        JLabel		etiqueta;
        String mensaje = null;
        
        switch(mode){
        	
	        case NEW_PARAMETER:
	        	setTitle("NUEVO OBJETO");
	        	mensaje = "�Que tipo de objeto desea insertar?";
	        	break;
	        
	        case EDIT_PARAMETER:
	        	setTitle("EDITAR OBJETO");
	        	mensaje = "�Que tipo de objeto desea modificar?";
	        	break;
        	
        }
        
        etiqueta = new JLabel(mensaje);
        etiqueta.setBounds(20, 5, 220, 30); // x, y, ancho, alto
        panel.add(etiqueta);
        
        // Radio buttons
        final ButtonGroup group = new ButtonGroup();
        
        optionModule.setBounds(20, 40, 120, 30); // x, y, ancho, alto
        group.add(optionModule);
        panel.add(optionModule);
        
        optionMethod.setBounds(20, 80, 120, 30); // x, y, ancho, alto
        group.add(optionMethod);
        panel.add(optionMethod);
        
        optionSentence.setBounds(20, 120, 120, 30); // x, y, ancho, alto
        group.add(optionSentence);
        panel.add(optionSentence);
        
        JButton botonVolver = new JButton();
        JButton botonGuardar= new JButton();
        
        botonVolver.setText("CANCELAR");
        botonVolver.setBounds(140, 180, 100, 30); // x, y, ancho, alto
        
        botonVolver.addActionListener(new ActionListener() {
        	
        	public void actionPerformed(ActionEvent event) {
        		frame.setVisible(true);
        		frame.dispose();
        	}
        	
        });
        panel.add(botonVolver);
        
        botonGuardar.setText("ACEPTAR");
        botonGuardar.setBounds(20, 180, 100, 30); // x, y, ancho, alto
        
        botonGuardar.addActionListener(new ActionListener() {
        	
        	public void actionPerformed(ActionEvent event) {
        		
        		switch(mode){
            	
    	        	case NEW_PARAMETER:
    	        		
    	        		// Dependiendo de la opcion de radio button se visualiza una ventana u otra...
    	        		if(optionModule.isSelected()){
    	        			
//    	        			frame.dispose();
//    	        			final ModuleMethodContainer moduleNew = new ModuleMethodContainer(ModuleMethodContainer.MODULO, ventana, con, mainContenedor, null);
//        	        		ventana.setContentPane(moduleNew.getMyContainer());
        	        		
    	        		}else if(optionMethod.isSelected()){
    	        			
//    	        			frame.dispose();
//    	        			final ModuleMethodContainer methodNew = new ModuleMethodContainer(ModuleMethodContainer.METODO, ventana, con, mainContenedor, null);
//        	        		ventana.setContentPane(methodNew.getMyContainer());
        	        		
    	        		}else if(optionSentence.isSelected()){
    	        			
    	        			frame.dispose();
        	        		final EditContainer mainNew = new EditContainer(null, ventana, prop, mainContenedor);
        	        		ventana.setContentPane(mainNew.getMyContainer());
    	        			
    	        		}
    	        		
    	        	break;
    	        
    	        	case EDIT_PARAMETER:
    	        	
    	        		if(optionModule.isSelected()){
    	        			frame.dispose();
    	        		}else if(optionMethod.isSelected()){
    	        			frame.dispose();
    	        		}else if(optionSentence.isSelected()){
    	        			
//    	        			customDialog = new EditDialog(objetoEditable,ventana, con);
//    	        			customDialog.setLocationRelativeTo(frame);
//    	        			customDialog.setResizable(false);
//    	        			customDialog.setVisible(true);
//    	        			ventana.setVisible(false);
    	        			
    	        			frame.dispose();
    	        			final EditContainer mainEdit = new EditContainer(objetoEditable,ventana, prop, mainContenedor);
        	        		ventana.setContentPane(mainEdit.getMyContainer());
    	        			
    	        		}
    	        		
    	        	break;
            	
        		}
    			
        	}
        	
        });
        panel.add(botonGuardar);
        
        add(panel);
        
	}
	
}
