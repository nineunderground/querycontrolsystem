package org.indrasoftwarelabs.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import org.indrasoftwarelabs.containers.EditContainer;

public class ArgumentDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	ArgumentDialog frame;
	public final	static int	NEW_PARAMETER	= 1;
	public final	static int	EDIT_PARAMETER	= 2;
	
	public ArgumentDialog(final DefaultTableModel model, final List<String> vectorParamsIDs, final String nombreParam, String tipoParam, final int fila, final int mode, final JButton boton){
		
		frame = this;
		
		// General values
    	setModalityType(ModalityType.APPLICATION_MODAL);
    	setTitle("INTRODUCE VALORES");
    	setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(300, 150);
        
        final JPanel panel = new JPanel();
        panel.setLayout(null);
        
        JLabel		etiqueta;
        
        etiqueta = new JLabel("PARAMETRO: ");
        etiqueta.setBounds(20, 5, 100, 30); // x, y, ancho, alto
        panel.add(etiqueta);
        
        
        final JTextField	campoNombre;
        
        if(nombreParam != null){
        	campoNombre = new JTextField(nombreParam);
        }else{
        	campoNombre = new JTextField();
        }
        
        etiqueta.setLabelFor(campoNombre);
        campoNombre.setSize(150, 30);
        campoNombre.setBounds(120, 10, 150, 30); // x, y, ancho, alto
        campoNombre.setEditable(true);
        campoNombre.setVisible(true);
        panel.add(campoNombre);
        
        JButton botonVolver = new JButton();
        JButton botonGuardar= new JButton();
        
        etiqueta = new JLabel("TIPO: ");
        etiqueta.setBounds(20, 40, 80, 30); // x, y, ancho, alto
        panel.add(etiqueta);
        
        Object[] listaValores = new Object[]{"STRING","NUMBER","DECIMAL","DATE","TIMESTAMP"};
        final JComboBox	tiposParametros = new JComboBox(listaValores);
        
        if(tipoParam != null){
        	tiposParametros.setSelectedItem(tipoParam);
        }
        
        tiposParametros.setSize(150, 30);
        tiposParametros.setBounds(120, 40, 150, 30); // x, y, ancho, alto
        tiposParametros.setEditable(false);
        tiposParametros.setVisible(true);
        panel.add(tiposParametros);
        
        botonVolver.setText("CANCELAR");
        botonVolver.setBounds(150, 80, 100, 30); // x, y, ancho, alto
        
        botonVolver.addActionListener(new ActionListener() {
        	
        	public void actionPerformed(ActionEvent event) {
        		frame.dispose();
        	}
        	
        });
        panel.add(botonVolver);
        
        botonGuardar.setText("ACEPTAR");
        botonGuardar.setBounds(40, 80, 100, 30); // x, y, ancho, alto
        
        botonGuardar.addActionListener(new ActionListener() {
        	
        	public void actionPerformed(ActionEvent event) {
        		
        		// Coger el valor seleccionado del combo y el nombre
        		String nombre = campoNombre.getText();
        		Object tipo = tiposParametros.getSelectedItem();
        		String valor = "";
        		
        		final String DATE_FORMAT_DATE		= "dd/MM/yyyy";
        		final String DATE_FORMAT_TIMESTAMP	= "dd/MM/yyyy HH:mm:ss";
        		DateFormat dateFormat;
        		Date date;
        	    
        		if(tipo.equals("DATE")){
        			
        			dateFormat = new SimpleDateFormat(DATE_FORMAT_DATE);
        	        date = new Date();
        			valor = dateFormat.format(date);
        			
        		}else if(tipo.equals("TIMESTAMP")){
        			
        			dateFormat = new SimpleDateFormat(DATE_FORMAT_TIMESTAMP);
        	        date = new Date();
        			valor = dateFormat.format(date);
        		}else{
        			valor = "0";
        		}
        		
        		System.out.println("Se a�ade el parametro " + nombre + " de tipo: " + (String)tipo);
        		
        		Object ob;
        		if(model.getRowCount() == 0){
        			ob = new String("0");
        		}else{
        			ob = model.getValueAt(model.getRowCount()-1, 0);
        		}
        		
        		String va;
        		int number = 0;
        		
        		try{
        			va = (String)ob;
        		}catch (Exception err){
        			va = String.valueOf( (Integer)ob );
        		}
        		
        		number = Integer.parseInt(va);
        		
        		boolean existe = false;
        		
        		switch(mode){
        		
	        		case NEW_PARAMETER:
	        			
	        			number = number+1;

	        			for(int cont=0; cont<model.getRowCount(); cont++){
	        				if( model.getValueAt(cont, 1).equals(nombre) ){
	        					existe = true;
	        					break;
	        				}
	        			}
	        			
	        			if(existe || nombre == null || nombre.length() == 0){
	        				String msg;
	    	        		JFrame popup;
	    	        		
	    	        		if(existe){
	    	        			msg = "Ya existe un parametro llamado: " +  nombre;
	    	        		}else{
	    	        			msg = "Debes introducir un nombre de parametro";
	    	        		}
	    	        		
	    					popup = new JFrame();
	    					popup.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	                    	popup.setSize(100, 100);
	                    	//JOptionPane.showMessageDialog(popup, msg, null, JOptionPane.WARNING_MESSAGE);
	                    	JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
	        			}else{
	        				model.addRow(new Object[]{number,nombre,tipo,valor});
		            		vectorParamsIDs.add(nombre + EditContainer.SEPARADOR + (String)tipo + EditContainer.SEPARADOR + "####");
		            		boton.setEnabled(false);
		            		frame.dispose();
	        			}
	        			
	        			break;
	        		case EDIT_PARAMETER:
	        			
	        			StringTokenizer tokens = new StringTokenizer(vectorParamsIDs.get(fila),EditContainer.SEPARADOR);
	        			String id = "";
	        			
	        			id = tokens.nextToken();
	        			id = tokens.nextToken();
	        			id = tokens.nextToken();

	        			for(int cont=0; cont<model.getRowCount(); cont++){
	        				if( !model.getValueAt(cont, 1).equals(nombreParam) && model.getValueAt(cont, 1).equals(nombre) ){
	        					existe = true;
	        					break;
	        				}
	        			}
	        			
	        			if(existe || nombre == null || nombre.length() == 0){
	        				String msg;
	    	        		JFrame popup;
	    	        		
	    	        		if(existe){
	    	        			msg = "Ya existe un parametro llamado: " +  nombre;
	    	        		}else{
	    	        			msg = "Debes introducir un nombre de parametro";
	    	        		}
	            			
	    					popup = new JFrame();
	    					popup.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	                    	popup.setSize(100, 100);
	                    	//JOptionPane.showMessageDialog(popup, msg, null, JOptionPane.WARNING_MESSAGE);
	                    	JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
	                    	
	        			}else{
	        				
	        				model.setValueAt(nombre, fila, 1);	// NOMBRE
		        			model.setValueAt(tipo, fila, 2);		// TIPO
		        			model.setValueAt(valor, fila, 3);	// VALOR
		        			
		        			vectorParamsIDs.remove(fila);
		        			vectorParamsIDs.add(nombre + EditContainer.SEPARADOR + (String)tipo + EditContainer.SEPARADOR + id);
		        			boton.setEnabled(false);
		        			frame.dispose();
		        			
	        			}
	        			
	        			break;
        		}
        		
        	}
        	
        });
        panel.add(botonGuardar);
        
        add(panel);
        
	}
	
}
