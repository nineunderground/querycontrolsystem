package org.indrasoftwarelabs.main;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.indrasoftwarelabs.containers.MainContainer;
import org.indrasoftwarelabs.containers.SearchContainer;
import org.indrasoftwarelabs.dialogs.SeleccionarDialog;
import org.indrasoftwarelabs.log.LogForDay;

public class MainForm {
	
	// Database parameters
	static String usr;
	static String pwd;
	static String sid;
	static String host;
	static String port;
	static String urlBase;
	static String DBDriver;
	
	// Frame components
//	private JScrollPane pane2;
	//private ListModel bigData;
	
//	private	JList	listMod;
//	private	JList	listMet;
//	private	JList	listSen;
//	
//	private	JLabel	labelMod;
//	private	JLabel	labelMet;
//	private	JLabel	labelSen;
	
	JPanel panel = new JPanel();
	JPanel panel2 = new JPanel();
	JPanel panel3 = new JPanel();
	
	ResultSet rs;
	String[] modules;
	String[] methods = new String[0];
	String[] queries = new String[0];
	
	/**
	 * Get properties file
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static Properties getPropertiesFile() throws FileNotFoundException, IOException {

		// configuracion de BBDD ORACLE de ./oracleconfig
		File setupBBDD = new File("DBproperties");
		
		if (!setupBBDD.exists()){
			
			setupBBDD = new File("bin/indrasoftwarelabs/DBproperties");
			
			if (!setupBBDD.exists()){
				LogForDay.writeToLog("No se ha encontrado el fichero de propiedades en la ruta: " + setupBBDD.getAbsolutePath() , QCS.logPath, LogForDay.ERROR);
				throw new FileNotFoundException("File not found");
			}
		}
			
		
		Properties properties = new Properties();
		properties.load(new FileInputStream(setupBBDD));
		
		return properties;
	}

	/**
	 * Sets initial panel
	 * @throws Exception 
	 */
	public void setPanelView(final Properties prop) throws Exception {
		
		final JFrame frame = new JFrame("QCS"); // Ventana principal del Query Control System
		
		//Create and set up the window.
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setSize(700, 800);
        
        final MainContainer main = new MainContainer(frame, prop);
        frame.setContentPane(main.getMyContainer());
        
        // VENTANA
        frame.addWindowListener(
        		
        		new WindowListener(){

					public void windowActivated(WindowEvent arg0) {
					}

					public void windowClosed(WindowEvent arg0) {
					}

					public void windowClosing(WindowEvent arg0) {
						
						int value = JOptionPane.showConfirmDialog(frame,"¿Quieres salir de la aplicacion?","SALIR",JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE);
	            		
	            		if(value == JOptionPane.OK_OPTION ){
	            			//MainForm.closeConexion(con);
	            			System.out.println("Conexion de BBDD cerrada");
	            			LogForDay.writeToLog("Aplicacion cerrada" , QCS.logPath, LogForDay.ERROR);
	    					System.exit(0);
	            		}
						
					}

					public void windowDeactivated(WindowEvent arg0) {
					}

					public void windowDeiconified(WindowEvent arg0) {
					}

					public void windowIconified(WindowEvent arg0) {
					}

					public void windowOpened(WindowEvent arg0) {
					}
        			
        		}
        		
        );
        
		/********************************************************************
        // MENU FILE
        /********************************************************************/
        
        // App icon
        //ImageIcon iconExit		=	null;
        ImageIcon iconNew		=	null;
        ImageIcon iconEdit		=	null;
        ImageIcon iconSearch	=	null;
        ImageIcon iconDelete	=	null;
        ImageIcon iconOrganize	=	null;
        
        ImageIcon iconSettings	=	null;
        ImageIcon iconLog		=	null;
        
        ImageIcon iconAbout		=	null;
        
		Image img;
		
		try {
			img = ImageIO.read(new File("res/icons/Info-icon.png"));
			iconAbout = new ImageIcon(img);
		} catch (IOException err) {
			LogForDay.writeToLog("No se encuentra la ruta res/icons/Info-icon.png " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
		}
		
		try {
			img = ImageIO.read(new File("res/icon.png"));
			frame.setIconImage(img);
		} catch (IOException err) {
			LogForDay.writeToLog("No se encuentra la ruta res/icons/icon.png " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
		}
		
		try {
			img = ImageIO.read(new File("res/icons/Add-icon.png"));
			iconNew = new ImageIcon(img);
		} catch (IOException err) {
			LogForDay.writeToLog("No se encuentra la ruta res/icons/Add-icon.png " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
		}
		
		try {
			img = ImageIO.read(new File("res/icons/Edit-icon.png"));
			iconEdit = new ImageIcon(img);
		} catch (IOException err) {
			LogForDay.writeToLog("No se encuentra la ruta res/icons/Edit-icon.png " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
		}
		
		try {
			img = ImageIO.read(new File("res/icons/Search-icon.png"));
			iconSearch = new ImageIcon(img);
		} catch (IOException err) {
			LogForDay.writeToLog("No se encuentra la ruta res/icons/Search-icon.png " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
		}
		
		try {
			img = ImageIO.read(new File("res/icons/Delete-icon.png"));
			iconDelete = new ImageIcon(img);
		} catch (IOException err) {
			LogForDay.writeToLog("No se encuentra la ruta res/icons/Delete-icon.png " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
		}
		
		try {
			img = ImageIO.read(new File("res/icons/Refresh-icon.png"));
			iconOrganize = new ImageIcon(img);
		} catch (IOException err) {
			LogForDay.writeToLog("No se encuentra la ruta res/icons/Refresh-icon.png " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
		}
		
		try {
			img = ImageIO.read(new File("res/icons/Tools-icon.png"));
			iconSettings = new ImageIcon(img);
		} catch (IOException err) {
			LogForDay.writeToLog("No se encuentra la ruta res/icons/Tools-icon.png " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
		}
		
		try {
			img = ImageIO.read(new File("res/icons/Folder-icon.png"));
			iconLog = new ImageIcon(img);
		} catch (IOException err) {
			LogForDay.writeToLog("No se encuentra la ruta res/icons/Folder-icon.png " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
		}
		
		JMenuBar menubar = null;
		JMenuItem eMenuItem = null;
		JMenu file = null;
		
		menubar = new JMenuBar();
		
		file = new JMenu("View");
		
		eMenuItem = new JMenuItem("NEW", iconNew);
		eMenuItem.setEnabled(true);
		
		eMenuItem.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		/**
            	 * TODO
            	 * Un cuadro de dialogo con la opcion de agregar un nuevo:
            	 * 
            	 * - MODULO
            	 * - METODO
            	 * - SENTENCIA
            	 * 
            	 */
            	
            	SeleccionarDialog customDialog = new SeleccionarDialog(SeleccionarDialog.NEW_PARAMETER, null, frame, prop, main.getMyContainer());
    			
            	customDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    			
    			customDialog.setLocationRelativeTo(frame);
    			customDialog.setResizable(false);
    			customDialog.setVisible(true);
        		
        	}
        });
		
		//file.add(eMenuItem);
		
		eMenuItem = new JMenuItem("EDIT", iconEdit);
		eMenuItem.setEnabled(true);
		
		eMenuItem.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		// Si no hay una query seleccionada, sacar un mensaje de dialogo advirtiendo del uso correcto
        		String valor = (String)main.listSen.getSelectedValue();
        		
        		if(valor == null){
        			String msg = "Por favor, selecciona una sentencia de la lista inferior";
            		JOptionPane.showConfirmDialog(frame,msg,"Atencion",JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
                	
        		}else{
        			
//        			EditDialog customDialog = new EditDialog(valor, frame, con);
//        			customDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
//        			
//        			customDialog.setLocationRelativeTo(frame);
//        			customDialog.setVisible(true);
//        			customDialog.setResizable(false);
        			
        			/**
                	 * TODO
                	 * Un cuadro de dialogo con la opcion del objeto que se desee editar:
                	 * 
                	 * - MODULO
                	 * - METODO
                	 * - SENTENCIA
                	 * 
                	 */
                	
                	SeleccionarDialog customDialog = new SeleccionarDialog(SeleccionarDialog.EDIT_PARAMETER, valor, frame, prop, main.getMyContainer());
        			customDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        			
        			customDialog.setLocationRelativeTo(frame);
        			customDialog.setResizable(false);
        			customDialog.setVisible(true);
        			
        		}
        		
        	}
        });
		
		//file.add(eMenuItem);
		
//		eMenuItem = new JMenuItem("Test", iconEdit);
//		eMenuItem.setEnabled(true);
//		
//		eMenuItem.addActionListener(new ActionListener() {
//        	public void actionPerformed(ActionEvent event) {
//        		
//        		OptionsTest test = new OptionsTest(con);
//        		ksetContentPane(test.getContainer());
//        		frame.setVisible(true);
//        		
//        	}
//        });
//		
//		file.add(eMenuItem);
		
		eMenuItem = new JMenuItem("DELETE", iconDelete);
		eMenuItem.setEnabled(false);
		//file.add(eMenuItem);
		
		eMenuItem = new JMenuItem("SEARCH", iconSearch);
		eMenuItem.setEnabled(true);
		
		eMenuItem.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
        		SearchContainer mainSearch = new SearchContainer(frame, prop, main.getMyContainer());
        		frame.setContentPane(mainSearch.getMyContainer());
        		frame.setVisible(true);
        		
        	}
        });
		
		//file.add(eMenuItem);
		
		eMenuItem = new JMenuItem("ORGANIZE", iconOrganize);
		eMenuItem.setEnabled(true);
		
		eMenuItem.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		
//        		OrganizeContainer mainOrganize = new OrganizeContainer(frame, con, main.getMyContainer());
//        		frame.setContentPane(mainOrganize.getMyContainer());
//        		frame.setVisible(true);
        		
        	}
        });
		
		//file.add(eMenuItem);
		
        //eMenuItem = new JMenuItem("EXIT", iconExit);
		eMenuItem = new JMenuItem("Settings", iconSettings);
        
        file.setMnemonic(KeyEvent.VK_F);
        
        eMenuItem.setMnemonic(KeyEvent.VK_C);
        eMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	
//            	try {
//            		
//            		int value = joptionpane.showconfirmdialog(frame,"¿quieres salir de la aplicacion?","salir",joptionpane.ok_option, joptionpane.question_message);
//            		
//            		if(value == joptionpane.ok_option ){
//            			mainform.closeconexion(con);
//            			system.out.println("conexion de bbdd cerrada");
//            			logforday.writetolog("aplicacion cerrada" , qcs.logpath);
//    					system.exit(0);
//            		}
//					
//				} catch (sqlexception e) {
//					e.printstacktrace();
//				} catch (classnotfoundexception e) {
//					e.printstacktrace();
//				}
				
            }

        });
        file.add(eMenuItem);
        
        file.addSeparator();
        
        eMenuItem = new JMenuItem("Log", iconLog);
        file.add(eMenuItem);
        
        menubar.add(file);
        
        JMenu help = new JMenu("Help");
        help.setMnemonic(KeyEvent.VK_H);
        
        eMenuItem = new JMenuItem("About us...", iconAbout);
        eMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                
            	Properties propertiesFile;
            	String version = "0.1";
				try {
					propertiesFile = MainForm.getPropertiesFile();
					version = propertiesFile.getProperty("QCS_VERSION");
				} catch (FileNotFoundException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
				} catch (IOException err) {
					LogForDay.writeToLog("Error: " + err.getLocalizedMessage(), QCS.logPath, LogForDay.ERROR);
				}
            	
            	String message = "Version " + version + "\nQuery Center System (QCS) Created by Indra Software Labs Madrid.\nThanks for use QCS!";
            	JOptionPane.showConfirmDialog(frame,message,"About us",JOptionPane.CLOSED_OPTION, JOptionPane.INFORMATION_MESSAGE);
            	
            }

        });
        help.add(eMenuItem);
        
        menubar.add(help);
        
        frame.setJMenuBar(menubar);
        frame.setLocationRelativeTo(null);
        
        frame.setVisible(true);
        frame.setResizable(false);
        
	}

	/**
	 * Get DB connection
	 * @param prop - Properties file with database parameters
	 * @return
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public static Connection getConexion(Properties prop) throws SQLException, ClassNotFoundException, Exception {
		
		LogForDay.writeToLog("Obteniendo conexion de BBDD" , QCS.logPath, LogForDay.DEBUG);
		
		Connection	c;
		
		usr		= prop.getProperty("DB_USER");
        pwd		= prop.getProperty("DB_PASS");
        sid		= prop.getProperty("DB_SID");
        host	= prop.getProperty("DB_HOST");
        port	= prop.getProperty("DB_PORT");
        urlBase	= prop.getProperty("DB_URL_BASE");
        DBDriver= prop.getProperty("DB_DRIVER");
        
        String url = urlBase + ":" + usr + "/" + pwd + "@" + host + ":" + port + ":" + sid;
        
        Class.forName(DBDriver);
        
        c = DriverManager.getConnection(url);
		
        c.setAutoCommit(false);
        
        LogForDay.writeToLog("Conexion de BBDD creada" , QCS.logPath, LogForDay.DEBUG);
        
		return c;
	}

	/**
	 * Close DB connection
	 * @param c
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public static boolean closeConexion(Connection	c) throws SQLException, ClassNotFoundException {
		
		boolean isClosed = false;
        c.close();
        isClosed = true;
		
        LogForDay.writeToLog("Conexion de BBDD cerrada" , QCS.logPath, LogForDay.DEBUG);
        
		return isClosed;
	}
	
}
