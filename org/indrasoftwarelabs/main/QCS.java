/**
 * 
 */
package org.indrasoftwarelabs.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import org.indrasoftwarelabs.log.LogForDay;

/**
 * @author irjimenez
 *
 */
public class QCS {

	static Connection conn;
	public static String logPath;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		try {
			
			File setupBBDD = new File("DBproperties");
			System.out.println("Probando ruta: " + setupBBDD.getAbsolutePath() );
			
			if (!setupBBDD.exists()){
				
				System.out.println("Probando ruta: " + setupBBDD.getAbsolutePath() );
				setupBBDD = new File("bin/indrasoftwarelabs/DBproperties");
				if (!setupBBDD.exists()){
					throw new Exception("File not found");
				}
				
			}
			
			Properties prop = new Properties();
			prop.load(new FileInputStream(setupBBDD));
			
			// Se prepara el log...
			logPath		= prop.getProperty("QCS_RUTA_LOGS");
			String valueLogMode = prop.getProperty("QCS_LOGS_LEVEL");
			
			if( !valueLogMode.equals("0") &&
				!valueLogMode.equals("1") &&
				!valueLogMode.equals("2") &&
				!valueLogMode.equals("3") &&
				!valueLogMode.equals("4") ){
				String msg = "Por favor revisa el valor del modo de log. solo puede tener valores:\n0[ERROR] - 1[WARNING] - 2[DEBUG] - 3[INFO] - 4[ALWAYS]";
	    		JOptionPane.showConfirmDialog(null,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
	    		return;
			}
			
			LogForDay.accessMode = Integer.parseInt(valueLogMode);
			
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;		            
		        }
		        LogForDay.writeToLog("Look & Feel tipo: " +  info.getName(), QCS.logPath, LogForDay.INFO);
		    }
			
			MainForm	gui	= new MainForm();
			gui.setPanelView(prop);
			
		} catch (SQLException err) {
			LogForDay.writeToLog("Aplicacion cerrada de forma forzosa. SQLException " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			String msg = "Hay un error. Por favor, revisa la conexion a la BBDD.";
    		JOptionPane.showConfirmDialog(null,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
		} catch(IOException err){
			LogForDay.writeToLog("Aplicacion cerrada de forma forzosa. IOException " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			String msg = "Hay un error en el properties. Por favor, revisa que existe el fichero propertiesDB y que su sintaxis es correcta.";
    		JOptionPane.showConfirmDialog(null,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
		} catch (Exception err) {
			LogForDay.writeToLog("Aplicacion cerrada de forma forzosa. Exception " + err.getLocalizedMessage() , QCS.logPath, LogForDay.ERROR);
			String msg = "Hay un error en la conexion a la BBDD. Por favor, revisa las conexiones.";
    		JOptionPane.showConfirmDialog(null,msg,"Error",JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
		}

	}

}
